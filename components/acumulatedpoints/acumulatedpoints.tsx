/* eslint-disable @next/next/no-img-element */
import React from "react"
import styles from "./acumulatedpoints.module.css"
import metrox from "../../img/mettroX.jpg"
import barras from "../../img/indice.png"
import { UserModel } from "../../types/basics"


export function AcumulatedPoints(props: UserModel) {

    const { data } = props

    return (
        <div className={styles.acumulated_points}>
            <div className={styles.greeting}>
                {/* <label>Hola</label> */}
                <label>{data.username}</label>
                {/* <label className="coma-span">tienes</label> */}
            </div>
            <div className={styles.container_kilometer}>
                <div className={styles.kilometers_counter}>
                    <img className={styles.decoration_img} src={barras.src} alt="" />
                    <label className={styles.tienes_web}>Acumula</label>
                    <h3> {data.travelKm} <span>Km</span></h3>
                    <label>{data.from} a {data.to}</label>
                </div>
                {/* <button className="free-travel-buttom">Conoce como viajar gratis</button> */}
            </div>
            <img className={styles.img_bus_user} src={metrox.src} alt="" />

        </div>
    )
}