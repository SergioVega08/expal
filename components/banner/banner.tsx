/* eslint-disable react/jsx-key */
/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import banner from "../../img/bannercali.png"
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';
import styles from "./banner.module.css";
import { url } from "inspector";
import { relative } from "path";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { SearchBanner } from "../searchbanner/searchbanner"
import { useMediaQuery } from 'react-responsive';


export function Banner(props: any) {
    const {
        images,
        tittle,
        subtittle,
        subtittlecolor,
        searchStatus,
        showTitle = true
    } = props

    const isMobile = useMediaQuery({ query: '(max-width: 767px)' })

    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);
    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === images.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? images.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex: any) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    const slides = images.map((item: any, index: number) => {
        console.log(item)
        const imageURL = typeof item == 'string' ? item : (isMobile ? item.movil : item.web)
        return (
            <CarouselItem>
                <a style={{ display: 'block' }} href={typeof item !== 'string' ? item.link : ''}>
                    <img className="bannersrc" src={imageURL} alt="fondo"
                        style={{ objectFit: "cover", objectPosition: "45% 50%", position: "relative", left: 0, right: 0, top: 0, bottom: 0, width: "100%", zIndex: -200 }}
                    />
                </a>
            </CarouselItem>
        )
    })

    return (
        <div className={styles.Banner}
            style={{ position: "relative" }}
        >
            <Carousel
                activeIndex={activeIndex}
                next={next}
                previous={previous}
                pause='hover'
            >
                {/* <CarouselIndicators items={img} activeIndex={activeIndex} onClickHandler={goToIndex} /> */}
                {slides}
                <CarouselControl className="" direction="prev" directionText="Previous" onClickHandler={previous} />
                <CarouselControl className="" direction="next" directionText="Next" onClickHandler={next} />
            </Carousel>

            <>
                {searchStatus && <SearchBanner style={{ position: 'absolute', margin: 'auto', top: "0", left: 0, right: 0, textAlign: 'center', zIndex: 50 }} preset='compact' />}
                {showTitle && <div className={`${styles['tittle-banner']} d-flex flex-column`} style={{ position: "relative", paddingTop: searchStatus ? "12vw" : "17vw" }}>
                    {tittle && tittle.split("/n").map((label: any, index: number) =>
                        <label key={index} className={styles.tittle}>{label}</label>
                    )}
                    <label className={styles.subtittle} style={{ color: subtittlecolor }}> {subtittle} </label>
                </div>}
            </>


        </div>
    );
}