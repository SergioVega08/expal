import React, {useState} from "react";
import barras from "../../img/barras-v-oscuro.svg"
import barrasVerdes from "../../img/barras-verdes.svg"
import barraOscura from "../../img/barras.svg"
import dirabajo from "../../img/flecha-abajo.svg"
import dirarriba from "../../img/flecha_arriba.svg"
import Image from 'next/image'
import styles from "./busdescription.module.css"
//import { TextFormatted } from "../../types/basics"

export interface BusDescriptionProps {
    tabLabels: string[]
    tabContent: string[]
}

const Card = ({ item, index, titleState }: { item: any, index: number, titleState: boolean }) => {
    return (
        <div className={styles.content_twofloor_bus}>
            <div className={styles.plusname_description} dangerouslySetInnerHTML={{ __html: item.value }}></div>
        </div>
    )
}
export function BusDescription(props: any) {
    
        const [tab, setTab] = useState(0)
    
        const { tabContent, tabLabels } = props;
    return(

    <div className="">
            {/* {twofloolstate ? */}

            <div className={styles.first_floor}>
                <div className={styles.two_floor_description}>
                    {tabContent.length >= 2 ?
                        <div className={styles.label_content}>
                            {tabLabels.map((label:string, index:number) =>
                                <div key={index} className={`${styles.tab_box} ${index != tab ? 'disabled' : ''}`} onClick={() => setTab(index)}>
                                    <div className={styles.tittles_second_floor} >
                                        <Image className={styles.barras_tittle} src={index != tab ? barraOscura : barras} alt="" />
                                        <div className={styles.principal_tabs}>
                                            <label className="" dangerouslySetInnerHTML={{ __html: label }}></label>
                                        </div>
                                    </div>
                                    <Image className={styles.floor_selector} src={index != tab ? dirabajo : dirarriba} alt="" />
                                </div>
                            )}

                        </div> : null}

                    <Card item={tabContent[tab]} index={tab} titleState={tabContent.length >= 2} />

                    <Image className={styles.barras_box} src={barrasVerdes} alt="" />
                </div>
            </div>
            {/* :

            <div className="single-bus-contaier">

                <div className="d-flex align-items-start">
                    <img className="barras-tittle" src={barras} alt="barras" />
                    <div className="d-flex flex-column justify-content-center bus-name">
                        <label className="bus-name-tittle">{busData.title}</label>
                        <label className="bus-name-subtittle">{busData.subtitle}</label>
                    </div>
                </div>

                <div className="bus-characteristics list-style">
                    <ul className="">
                        {busData.list.map((item) =>
                            <li className="characteristics">{item}</li>
                        )}
                    </ul>
                </div>

                <img className="barras-box" src={barrasVerdes} alt="" />
            </div>

            } */}
        </div>

    )
}