/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react"
import styles from  "./buspropietiesimages.module.css"
import { Image } from "../../types/basics"
// import { getImageURL } from "../../utils"
import Lightbox from 'react-image-lightbox';
import { useSiteData } from "react-static";

export interface BusPropietiesImagesProps {
    images: Image[]

}

export function BusPropietiesImages(props: any) {
    const { images } = props
    const { contentURL } = useSiteData()

    const [isOpen, setisOpen] = useState(false)
    const [photoIndex, setphotoIndex] = useState(0)

    const imageURLs = images.map((image: { uri: { url: any; }; }) => contentURL + image.uri.url)
    // console.log(imageURLs)
    return (
        <div className={styles.container_bus_propieties}>
            <div className={styles.row}>
                {imageURLs.map((image: string, index:number) =>
                    <a key={index} onClick={() => { setphotoIndex(index); setisOpen(true) }}>
                        <img className={styles.bus_ilustration} src={image} alt="" />
                    </a>
                )}
            </div>
            <div className={styles.images_background}></div>
            {isOpen &&
                <Lightbox
                    mainSrc={imageURLs[photoIndex]}
                    nextSrc={imageURLs[(photoIndex + 1) % images.length]}
                    prevSrc={imageURLs[(photoIndex + images.length - 1) % images.length]}
                    onCloseRequest={() => setisOpen(false)}
                    onMovePrevRequest={() => setphotoIndex((photoIndex + images.length - 1) % images.length)}
                    onMoveNextRequest={() => setphotoIndex((photoIndex + 1) % images.length)}
                />
            }
        </div>
    )
}
