/* eslint-disable @next/next/no-img-element */
import React from "react"
import { Route,Image,Bus } from "../../types/basics"
import { BusDescription } from "../busdescription/busdescription"
import {BusPropietiesImages } from "../buspropietiesimages/buspropietiesimages"
import { Rutas } from "../rutas/rutas"
import styles from "./choicedbus.module.css"

// export interface ChoicedBusProps {
//     bus: Bus
//     logo: Image
//     image: Image
//     serviceImages: Image[]
//     title: string
//     data: Route[]
// }

const bus={
    bus: "S26",
    subTitle: "para viajer doblemente seguro",
    description: "Dotados con tecnología para su entretenimiento, con un ambiente interno amplio que proporciona confort y seguridad a los viajeros.",
    services: ["44 sillas reclinables",
    "Sistema Individual de Entretenimiento Digital Disfruta de contenidos, películas, música y aplicaciones",
    "Aire Acondicionado",
    "Luz Individual: ",
   " Para mejor comodidad, nuestro bus está equipado con luces individuales.",
    "Dos conductores calificados:Para tu tranquilidad las rutas de trayectos largos son operadas por 2 conductores.",
   " Conexión WI-FI: Accede de manera ilimitada a los servicios de Chat, Mail, y Redes sociales.",
    "G.P.S Seguimiento y Control Satelital",
    "Conectores: Conectores de energía para tus dispositivos.",
     "Baño"
    ],
    tabs: ["Primer Piso"]
}
 const image = {
     src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/principal-s26max.png"
 }

 const serviceImages =[
    {
       src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-10.jpg" 
    },
    {
        src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-09.jpg" 
    },
    {
        src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-01.jpg" 
    },
    {
        src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-05.jpg" 
    },
    {
        src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-02.jpg" 
    },
    {
        src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-06.jpg" 
    }
 ]

export function ChoicedBus(props: any) {
    const { bus, logo, image, serviceImages, data, title } = props

    return (
        <div className={styles.choiced_bus}>
            <div className={styles.bloque}>
                <div className={styles.flex}>
                    <div className={styles.bus_choiced_container}>
                        <div className={styles.img_bus}>
                            <img className={styles.img_bus_choiced} src={image} alt="" />
                            <img className={styles.icon_bus_choiced} src={(logo ? logo.uri.url : '')} alt="" />
                        </div>
                        <BusDescription tabLabels={bus.tabs} tabContent={bus.services} />
                    </div>
                    <div className={styles.right_column}>
                        <div className={styles.tittle_bus_choiced}>
                            <h1>{bus}</h1>
                            <h3>{bus.subTitle}</h3>
                        </div>
                        <div className={styles.description_bus_choiced}>
                            <p dangerouslySetInnerHTML={{ __html: bus.description && bus.description}}></p>

                        </div>
                        <BusPropietiesImages images={serviceImages} />
                        {/* <Rutas routes={data} title={title} preset='compact' columns={['src', 'dst']} /> */}
                    </div>

                </div>
            </div>
            <div className="d-block d-md-none">
                <div className={styles.tittle_bus_choiced_movil}>
                    <h1>{bus}</h1>
                    <h3>{bus.subTitle}</h3>
                </div>

                <img className={styles.img_bus_choiced_web} src={image} alt="" />

                <div className={styles.flex}>
                    <img className={styles.icon_bus_choiced_web} src={(logo ? logo.uri.url : '')} alt="" />
                    <p className={styles.description_bus_choiced_web} dangerouslySetInnerHTML={{ __html: bus.description && bus.description }}></p>
                </div>
                <BusPropietiesImages images={serviceImages} />
                <BusDescription tabLabels={bus.tabs} tabContent={bus.services} />
                <Rutas routes={data} title={title} preset='compact' />
            </div>

        </div>
    )
}