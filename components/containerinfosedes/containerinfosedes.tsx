/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from "react"
import styles from "./containerinfosede.module.css"
import { InfoSede } from "../infosede/infosede"
import { ListSedes } from "../listsedes/listsedes"
import { RutasIndicator } from "../rutasindicator/rutasindicator"
import { Sede, Route } from "../../types/basics"
// import { ListDropdown } from "components/listdropdownBeta/ListDropdown"
import { useRouteData } from "react-static"
import { getImages} from "../../utils/images"
import _ from "lodash"
import { Carousel, CarouselIndicators, CarouselControl, CarouselItem, CarouselCaption } from "reactstrap"
import { ListDropdown } from "../listdropdownBeta"

// export interface renderItemProps {
//     index: number
//     item: Sede[]
// }

export interface ContainerInfoSedesProps {
    // sucursales: JSONApiBody<Sede>[]
}
const rutas = [
    {
      src:"Cali",
      dts:"Armenia"
    },
    {
      src:"Cali",
      dts:"Bogotá"
    },
    {
      src:"Cali",
      dts:"Buga"
    },
    {
      src:"Buga",
      dts:"Cali"
    },
    {
      src:"Medellin",
      dts:"Cali"
    },
    {
      src:"Cali",
      dts:"Bogota"
    },
    {
      src:"Cali",
      dts:"Medellin"
    },
    {
      src:"Armenia",
      dts:"Cali"
    },
    {
      src:"Cali",
      dts:"Tuluá"
    },
    {
      src:"Tuluá",
      dts:"Cali"
    },
    {
      src:"Cali",
      dts:"Pereira"
    },
    {
      src:"Pereira",
      dts:"Cali"
    },
    {
      src:"Cali",
      dts:"Manizales"
    },
    {
      src:"Manizales",
      dts:"Cali"
    }

] 

const sedesInfo =[
    { title:"Armenia",
      sede:{
          city:"Armenia",
          location:"Armenia (Quindio)",
          address:"Terminal de Transportes Cl 35 # 20-68 Barrio Santander Local 16 - 18",
          officeHours:"Lunes a Domingo: 5:30 AM - 10:30 PM",
          phone:"+573147909069"
      },
      images:{
        value :"",
        url:""
      }
    },
    { title:"Bogotá",
    sede:{
        city:"Bogotá",
        location:"Bogotá (Bogotá D.C )",
        address:"Dirección: Terminal Transportes Salitre Dg 23 # 69-11 modulo amarillo local 111",
        officeHours:"Lunes a Domingo: 24 Horas",
        phone:"+573105451772"
    },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Buenaventura",
    sede:{
        city:"Buenaventura",
        location:"Buenaventura (Valle del cauca )",
        address:"Terminal de transportes Cra. 5 #7-230 P1  Local 37 Barrio obrero",
        officeHours:"Temporalmente fuera de servicio.",
        phone:"0"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Buga",
    sede:{
        city:"Buga",
        location:"Buga (Valle del cauca )",
        address:"Terminal Transporte Calle 4ta 23-91 local 14",
        officeHours:"Lunes a Domingo: 5:00 AM - 8:00 PM ",
        phone:"+573147909064"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Caicedonia",
    sede:{
        city:"Caicedonia",
        location:"Caicedonia (Valle del cauca)",
        address:"Calle 13 Cra 15 plaza de mercado local 028",
        officeHours:"Lunes a Domingo: 06:00 a.m. - 5:00 p.m.",
        phone:"+573147909060"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Cali",
    sede:{
        city:"Cali",
        location:"Santiago de Cali (Valle del cauca)",
        address:"Calle 30 Norte # 2 AN -- 29 P2-3 Local 309",
        officeHours:"Lunes a Domingo: 24 Horas",
        phone:"+573147909085"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Cartago",
    sede:{
        city:"Cartago",
        location:"Cartago (Valle del cauca)",
        address:"Dirección: Cra 2ª 1t-12 Barrio horizonte sector los almendros",
        officeHours:"Lunes a Domingo y festivos: 07 a.m. a 06:00 p.m.",
        phone:"+573147909080"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Ibague",
    sede:{
        city:"Ibague",
        location:"Ibagué (Tolima)",
        address:"Terminal de Transportes Cr 2 # 20 - 86  Local 169",
        officeHours:"Lunes a Domingo: 24 horas ",
        phone:"+573147909070"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Manizales",
    sede:{
        city:"Manizales",
        location:"Manizales (Caldas )",
        address:"Dirección: Terminal Transportes Cr 43 # 65-100 Sector Los Cambulos local 10 y 11",
        officeHours:"Lunes a Domingo: 4:30 AM - 8:30 PM",
        phone:"+573147909067"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Medellín",
    sede:{
        city:"Medellín",
        location:"Medellín (Antioquia)",
        address:"Terminal Transporte Sur Cr 65 Cl 8B - 91 Local 35",
        officeHours:"Lunes, martes, miércoles, jueves y sábado: 11:00 a.m. - 09:30 p.m. Viernes y domingo: 2:00 p.m. - 09:30 p.m.",
        phone:"+573108982921"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Palmira",
    sede:{
        city:"Palmira",
        location:"Palmira ( Valle del cauca)",
        address:"Calle 42 # 28-37 (palmira versalles)",
        officeHours:"Lunes a Domingo: 4:30 AM - 9:00 PM",
        phone:"+573147909086"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Pereira",
    sede:{
        city:"Pereira",
        location:"Pereira (Risaralda)",
        address:"Terminal de Transportes Cl 17 # 23-157 Local 121 – 137",
        officeHours:"Lunes a Domingo:  05:00 AM - 11:00 PM ",
        phone:"+573147909074"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Popayán",
    sede:{
        city:"Popayán",
        location:"Popayán (Cauca)",
        address:"Terminal de Transportes Transv. 9 # 4N-125 Local 04",
        officeHours:"Lunes a Domingo:  5:30 AM - 7:30 PM",
        phone:"+573147909078"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Sevilla",
    sede:{
        city:"Sevilla",
        location:"Sevilla (Valle del cauca)",
        address:"Dirección: Cra 48 # 53-01 Esquina",
        officeHours:"Lunes a Domingo: 6:00 AM - 6:00 PM",
        phone:"+573147909063"
      },
      images:{
        value :"",
        url:""
      },
    },
    { title:"Tuluá",
    sede:{
        city:"Tuluá",
        location:"Tuluá (Valle del cauca)",
        address:"Terminal de Transportes Transversal 12 #39A - 35",
        officeHours:"Lunes a Domingo: 4:30 AM - 11:00 PM",
        phone:"+573147909065"
      },
      images:{
        value :"",
        url:""
      },
    }
]

const listaSedes=
    ["armenia",
    "bogotá",
    "buenaventura",
    "buga",
    "caicedonia",
    "cali",
    "cartago",
    "ibague",
    "manizales",
    "medellín",
    "palmira",
    "pereira",
    "popayan",
    "sevilla",
    "tuluá"]

export function ContainerInfoSedes(props: any) {
    // const { sucursales } = props


    const { sedesInfo: sedesData, rutas: routeData } = useRouteData()
    if (!sedesData && !routeData) return null

    // const sedes: Sede[] = Object.values(sedesData.nodeSede)
    // const routes: Route[] = Object.values(routeData.nodeRoute)

    // console.log(sedes)

    const sucurGrouped = _.groupBy(sedesInfo, sucur => sucur.sede.city.toLowerCase())

    const listSedes = Object.keys(sucurGrouped).sort()
    const [cityIndex, setcityIndex] = useState(listSedes[0])

    const sedesg = sucurGrouped[cityIndex]

    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === sedesg.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? sedesg.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex: number) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    const renderItem = ({ item, index }: any) => {

        // const [i, seti] = useState(0)
        // setTimeout(() => seti((i * 1) % item.length), 3000)
        const i = 0
        const sede = item[i]

        const routesFiltered = rutas
            .filter(r =>
                (r.src) === (sede.city) ||
                (r.dts) === (sede.city))

        return (<>
            {item.map((sede: { city: any }, index: number) => <InfoSede key={index} title={`${sede.city} ${index + 1}`} sede={sede} />)}
            <RutasIndicator routes={routesFiltered} />
        </>
        )
    }

    const routesFiltered = _.uniqBy(rutas
        .filter(r =>
            (r.src) === (sedesg[0].title) ||
            (r.dts) === (sedesg[0].title))
        , (elem) => elem.src + elem.dts)

    const slides = sedesg.map((sede, index) => {
        const images = getImages(sedesData, sede, 'image')

        return (
            <CarouselItem
                onExiting={() => setAnimating(true)}
                onExited={() => setAnimating(false)}
                key={index}
            >
                {sedesInfo.map((sede, index)=> 
                {<InfoSede key= {index} title={sede.title} sede={sede.sede} images={sede.images}/>})}
            </CarouselItem>
        );
    });

    return (
        <div className="container-info-sedes">
            <div className={styles.bloque}>
                <div className={styles.flex}>
                    <ListSedes sucursales={listaSedes} onCity={setcityIndex} />
                    <div className={styles.info_container}>
                        <Carousel
                            className={styles.selector_city_sede}
                            activeIndex={activeIndex}
                            next={next}
                            previous={previous}
                        >
                            {/* <CarouselIndicators items={sedesg} activeIndex={activeIndex} onClickHandler={goToIndex} /> */}
                            {slides}
                            {/* <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} /> */}
                            {/* <CarouselControl direction="next" directionText="Next" onClickHandler={next} /> */}
                        </Carousel>
                    </div>
                </div>
                <RutasIndicator routes={routesFiltered} />
            </div>


            <div className={styles.container_indo_sedes_movil}>
                 {/* <ListDropdown data={Object.entries(sucurGrouped).sort().map(([key, value]) => ({ label: _.startCase(key), value: value }))} renderItem={renderItem} />  */}
            </div>
        </div>

    )
}