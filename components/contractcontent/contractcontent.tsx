import React from "react"
import style from "./contractcontent.module.css"
import { ListDropdown } from "../listdropdownBeta"
import pdf from "../../img/pdf.png"
import xpath from 'xpath'
import { DOMParser as dom } from 'xmldom'


const title = `<h3>Contrato de transporte</h3>`



export function ContractContent({ data }: any) {

    const body = data.body
    const html = data.dropdown
    const sections = body.split('<hr />');
    const doc = new dom().parseFromString(html);
    const labels = xpath.select("//h3", doc)
    const values = xpath.select("//h3/following-sibling::*[1]", doc)
    const listDropdown = labels.map((label: any, index) => {
        const value = values[index]
        return label ? { label: label.firstChild.data, value: [value.toString()] } : null
    }).filter(v => !!v)
    // const { listDropdown } = getStructHTML(html)

    return (
        <div className={style['container-contract']}>
            <div>
                <div className="d-md-flex align-items-center">
                    <div className={style['content-contract']} dangerouslySetInnerHTML={{ __html: title }}>

                    </div>
                    <div className="d-none d-md-block">
                        <button className={`d-flex align-items-center ${style['buttom-pdf']}`}>
                            <a href="https://cms.expal.com.co/sites/default/files/2020-12/CONTRATO%20DE%20TRANSPORTE%20EXPAL.pdf" download>
                                <img src={pdf.src} alt="img pdf" />
                                <div className="d-md-flex flex-column">
                                    <label>Descargar</label>
                                    <label>contrato</label>
                                </div>
                            </a>
                        </button>
                    </div>

                </div>
                <div className={style['content-contract']} dangerouslySetInnerHTML={{ __html: sections[0] }}></div>
                <div className="d-block d-md-none">
                    <button className={`d-flex align-items-center ${style['buttom-pdf']}`}>
                        <a href="https://cms.expal.com.co/sites/default/files/2020-12/CONTRATO%20DE%20TRANSPORTE%20EXPAL.pdf" download>
                            <img src={pdf.src} alt={`${style['img']} ${style['pdf']}`} />
                            <div className="d-md-flex flex-column">
                                <label>Descargar</label>
                                <label>contrato</label>
                            </div>
                        </a>
                    </button>
                </div>
            </div>
            <ListDropdown
                data={listDropdown}
            />
        </div>

    )
}