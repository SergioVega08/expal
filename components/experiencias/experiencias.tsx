/* eslint-disable @next/next/no-img-element */
import React from "react";
import "./experiencias.module.css";
import bannerMujeres from "../../img/nosotros-mujeres.png";
import styles from "./experiencias.module.css"

export function Experiencias() {

    const subBannerNosotros = [
        { img: bannerMujeres.src},
        { description: <p>Generamos experiencias de viaje a mas de <span>5 millones</span>  de pasajeros al año, los cuales disfrutan del mejor confort y entretenimiento a bordo.</p> }
    ]
    return (
        <div className={styles.banner_bottom}>
            <div className={styles.banner_bottom_img}>
                {subBannerNosotros.map((item, index) =>
                    <img key ={index} src={item.img} alt="" />
                )}

            </div>
            <div className={styles.col_mid}></div>
            <div className={styles.description_banner_bottom}>
                {subBannerNosotros.map((item) =>
                    item.description
                )}
            </div>
        </div>
    );
}