/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import React from "react";
import styles from "./footer.module.css";
import tell from "../../img/telefono-verde.svg"
import cell from "../../img/celular-verde.svg";
import fijo from "../../img/telefono-fijo.svg"
// import whatt from "../../img/whastapp-verde.svg";
import logoss from "../../img/tira_logos.png";
import cert from "../../img/buroVeritas.png";
import supertra from "../../img/superTransporte.png";
import Link from 'next/link';
import { FooterLinks } from "../../components/insidefootericons/insidefootericons"
import Image from 'next/image'

export function Footer(props: any) {
    const column1 = [{
        'name': "Sobre Expreso Palmira",
        'link': "/nosotros",
    },
    {
        'name': "Cliente Ultra",
        'link': "/ultra",
    },
    {
        'name': "Sedes",
        'link': "/sedes",
    },
    {
        'name': "Términos y Condiciones",
        'link': "/servicio-cliente/politica",
    },
    {
        'name': "PQRS",
        'link': "https://www.expresopalmira.net/transporte/pqr.php",
    },
    ]
    const column2 = [{
        'name': "Contrato de transporte",
        'link': "/servicio-cliente/contrato",
    },
    {
        'name': "Preguntas Frecuentes",
        'link': "/servicio-cliente/preguntas",
    },
    {
        'name': "Viaje con menores",
        'link': "/servicio-cliente/menores",
    },
    {
        'name': "Transporte de mascotas",
        'link': "/servicio-cliente/mascotas",
    },
    {
        'name': "Tiquetes en Línea",
        'link': "/servicio-cliente/onlinetiquets",
    },
    {
        'name': "Manejo de equipaje",
        'link': "/servicio-cliente/equipaje",
    },
    ]
    const column3 = [{
        'name': "Viajar a Bogotá",
        'link': "/rutas",
    }
    ]
    const menuName = 'footer'
    // console.log(childrenMap)

    const FooterMenu = ({ title, object }: { title: string, object: any }) => {
        // console.log(menu)
        return (<>
            <label>{title.toUpperCase()}</label>
            {object.map((item: any, index: number) =>
                <Link key={index} href={item.link}>{item.name}</Link>)}
        </>
        )
    }

    return (
        <div className="footer d-flex flex-column">
            <div className={styles.contacto}>
                <div className={`${styles['contenedor-footer']} d-flex flex-wrap justify-content-center flex-column flex-md-row`}>
                    <div className={`${styles['titulo-contacto']} margin-md`}>
                        <label className={styles.comu}>Comunícate</label>
                        <label className={styles.nosotros}>con nosotros</label>
                    </div>

                    <a href="tel:018000936662" className={`${styles.lineaNacional} margin-md`}>
                        <div className={styles.linImg}>
                            <Image src={tell} alt="" />
                        </div>
                        <div className={styles.lintxt}>
                            <label>Línea Nacional: <br></br>01 8000 93 66 62</label>
                        </div>
                    </a>

                    <a href="tel:3110640" className={`${styles.fijo} margin-md`}>
                        <div className={styles.celimg}>
                            <Image src={fijo} alt="" />
                        </div>
                        <div className={styles.celtxt}><label>Fijo:<br></br>(+2) 3 110 640</label> </div>
                    </a>

                    {/* <a href="tel:3112730419" className="celular margin-md ">
                        <div className="celimg"><img src={cell}></img> </div>
                        <div className="celtxt"><label>Celular:<br></br>(+57) 311 273 04 19</label> </div>
                    </a> */}

                    {/* <a href="https://wa.me/573105452791?text=Hola%2C%20quiero%20hacer%20una%20pregunta%3A" className="whatsapp margin-md ">
                        <div className="whatsapp-img"><img src={whatt}></img> </div>
                        <div className="whatsapp-txt"><label>WhatsApp:<br></br>(+57) 310 545 27 91</label></div>
                    </a> */}
                </div>

            </div>
            <div className={styles.sfoot}>
                <div className={styles['footer-menu'] + " " + styles.exppall}>
                    {<FooterMenu title="EXPRESO PALMIRA" object={column1} />}
                </div>
                <div className={styles['footer-menu'] + " " + styles.antdvi}>
                    {<FooterMenu title="ANTES DE VIAJAR" object={column2} />}
                </div>
                <div className={styles['footer-menu'] + " " + styles.desyrut}>
                    {<FooterMenu title="DESTINOS Y RUTAS" object={column3} />}
                </div>
                <div className={styles.metdpag}>
                    <div className={styles.smetdpag}>
                        {/* <div className="smettxt"><label>Paga fácil, rápido y seguro<br></br> desde cualquier lugar.</label></div> */}
                        <div className={styles.smettxt}>
                            <label>
                                Si viaja entre Cali y Bogotá <br />
                                <a href="#" style={{ color: '#008000' }}>esta información es importante</a>
                            </label></div>
                        {/* <div className="smeimg"><img src={logoss}></img></div> */}
                        <FooterLinks />
                    </div>
                </div>
            </div>
            <div className={styles.copyright}>
                <div className="d-flex align-center container justify-content-around align-items-center">
                    <div className={styles.derechos}>
                        <label>Transportes Expreso Palmira S.A. 2021 © Todos los derechos reservados.</label>
                    </div>
                    <div className="d-none d-md-block">
                        <div className="d-flex">
                            <img className={`${styles['footer-img']} border-right`} src={cert.src}></img>
                            <img className={styles['footer-img']} src={supertra.src}></img>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

