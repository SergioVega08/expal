/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable @next/next/link-passhref */
/* eslint-disable react/jsx-key */
import React, { useState } from "react";
import facebook from "../../img/facebook.svg";
import insta from "../../img/instagram.svg";
import twitt from "../../img/twitter.svg";
import maila from "../../img/mailing.svg";
import carrito from "../../img/carrito_compras.svg";
import lupa from "../../img/lupa.svg";
import 'bootstrap/dist/css/bootstrap.css';
import ep from "../../img/logo.png";
import mascota from "../../img/mascota.svg"
import usuario from "../../img/usuario.svg";
import usuariogris from "../../img/usuario-gris.svg"
import burger from "../../img/menu-button.svg";
import callcenter from "../../img/call_center_contacto.svg";
import Image from 'next/image'
import head from "next/head";
import Link from 'next/link'
import styles from './header.module.css';
import Dropdown from 'react-bootstrap/Dropdown';
import { NavMenu } from "../nav-menu/navMenu";
//Dejeme 
export default function Header() {
    const [sitedrawopen, setSitedrawopen] = useState(false)
    const _toggleNav = () => {
        setSitedrawopen(true)
        setTimeout(() => setSitedrawopen(false), 100)
    }
    // const { data: dat } = props
    // if (!dat) return null

    const MenuItem = ({ isDropDown, title, childrens, route }:
        { isDropDown: boolean, title: string, childrens: any, route: string }) => {
        const [dropdownOpen, setDropdownOpen] = useState(false);
        const toggle = () => setDropdownOpen(!dropdownOpen);
        return (
            <Dropdown>
                <>
                    {
                        isDropDown ?
                            <>
                                <a className={`dropdown-toggle ${styles.fontWeight}`} onMouseEnter={toggle}>{title.toUpperCase()}</a>
                                <div className={`dropdown-menu ${dropdownOpen ? 'show' : ''}`} onMouseLeave={toggle}>
                                    {childrens.map((item: any, index: number) => {
                                        return (<div className={`dropdown-item ${styles.itemHov} ${styles['dropdown-item']}`}>
                                            <Link key={index} href={item.route}>{item.name}</Link>
                                        </div>);
                                    })}
                                </div>
                            </>
                            :
                            <div className={styles.fontWeight}>
                                <Link href={route}>{title.toUpperCase()}</Link>
                            </div>
                    }
                </>
            </Dropdown>
        )
    }
    return (
        <div className={styles.seccion}>
            <div className={styles.header}>
                <header className={styles.web_header}>
                    <div className={styles.redes}>
                        <div className={styles.mail}>
                            <a href="mailto:servicioalcliente@expresopalmira.com.co"><Image width={15} height={15} src={maila} alt="Mail Expreso Palmira" /></a>
                            <a className={styles.email} id="servicio" href="mailto:servicioalcliente@expresopalmira.com.co">servicioalcliente@expresopalmira.com.co</a>
                        </div>
                        <div className={styles.socialMedia}>
                            <div ><a href="https://www.facebook.com/Expresopalmiraoficial/"><Image width={15} height={15} src={facebook} alt="Facebook Expreso Palmira" /></a></div>
                            <div ><a href="https://www.instagram.com/expresopalmira_/"><Image width={15} height={15} src={insta} alt="Instagram Expreso Palmira" /></a></div>
                            <div ><a href="https://twitter.com/ExpresoPalmira"><Image width={15} height={15} src={twitt} alt=" Twitter Expreso Palmira " /></a></div>
                        </div>
                        <div className={styles.iniciarsesion}>
                            <Link href="/userpage" >
                                <div className={styles.ins} ><label id="a">Iniciar Sesión</label></div>
                            </Link>
                            <Link href="/userpage" >
                                <div className={styles.kol} > <Image width={20} height={20} id="img" src={usuario} alt="Usuario" /></div>
                            </Link>
                        </div>

                    </div>

                    <div className={styles['header-nav']}>
                        <div className={styles.logo_ep}><Link href="/" passHref><Image layout="responsive" width={12} height={4} src={ep} alt="Logo Expreso Palmira" /></Link></div>
                        <div className={styles.relleno_border}></div>
                        <div className={styles.navigation}>
                            <div id={styles.nav}>
                                <MenuItem isDropDown={false} title="INICIO" childrens={null} route="/"/>
                                <MenuItem isDropDown={true} title="NOSOTROS" route={""} childrens={[
                                    {
                                        name: "NUESTRA HISTORIA",
                                        route: "/nosotros",
                                        cName: "dropdownItem"
                                    },
                                    {
                                        name: "FUNDACIÓN",
                                        route: "/fundacion",
                                        cName: "dropdownItem"
                                    },
                                    {
                                        name: "SEDES",
                                        route: "/sedes",
                                        cName: "dropdownItem"
                                    },
                                ]}
                                />
                                <MenuItem isDropDown={false} title="RUTAS" childrens={null} route="/rutas" />
                                <MenuItem isDropDown={true} title="BUSES" route="" childrens={[

                                    {
                                        name: "S26",
                                        route: "/bus"

                                    },
                                    {
                                        name: "METTRO",
                                        route: "/bus"

                                    },
                                    {
                                        name: "METTRO X",
                                        route: "/bus"

                                    },
                                    {
                                        name: "S26 MAXXI DUPPLO",
                                        route: "/bus"

                                    },

                                ]}
                                />
                                <MenuItem isDropDown={false} title="SERVICIO AL CLIENTE" childrens={null} route="/servicio-cliente" />
                                <MenuItem isDropDown={false} title="PQRS" childrens={null} route="https://www.expresopalmira.net/transporte/pqr.php" />

                            </div>
                        </div>
                        <div className={styles.relleno_border}></div>
                        <div className={styles.icon_carrito_lupa}>
                            <div className={styles.carrito}><a href="https://expresopalmira.pinbus.com/"><Image src={carrito} alt="Carrito" /></a></div>

                            <div className={styles.lupa}><Link href="/servicio-cliente/preguntas" passHref><Image src={lupa} alt="Lupa Expreso Palmira" /></Link></div>
                        </div>
                    </div>
                </header>
            </div>

            <div className={`${styles['responsive-header']} d-xs-block d-md-none d-flex justify-content-between align-items-center`}>
                <div className={styles['logo-ep-responsive']}>
                    <Link href="/" passHref><Image src={ep} alt="Expreso Palmira" /></Link>
                </div>
                <div className="d-flex">
                    <button className={styles['burger-buttom']}onClick={_toggleNav}><Image src={burger} alt="burger" /></button></div>
                <NavMenu show={sitedrawopen}/>

            </div>
        </div>


    );

}
// ${styles.dropdownToggle} ${styles.show}