/* eslint-disable @next/next/no-img-element */
import React from "react";
import styles from "./historia.module.css";
import Image from 'next/image'
import busNosotros from "../../img/nosotros-busbg.png";
import mujeresNosotros from "../../img/nosotros-mujeres.png";
import { HistoriaProps } from "../../types/basics";


export function Historia(props: HistoriaProps) {
    const {img,
          title,
          topImage,
          mainDescription,
          leftDescription,
          rigthDescription
    } = props

    return (
        <div className={styles.main_historia}>
            <div className={styles.descripcion_ep_web}>
                <div className={styles.descripcion_content}>
                    <div className={styles.col_4}>
                        <div className={styles.imgBusNosotros} ><img src={img} alt="" /></div>
                    </div>

                    <div className={styles.historia_content}>

                        <div className={styles.titulo_historia}>
                            <label>{title}</label>
                        </div>

                        {topImage ?
                            <div className={styles.image_separator}>
                                <div className={styles.line_separator}></div>
                                <img className={styles.top_image} src={topImage} alt="" />
                                <div className={styles.line_separator}></div>
                            </div>
                            : null
                        }


                        <div className={styles.historia} dangerouslySetInnerHTML={{ __html: mainDescription }}>
                                            </div>

                        {/* <div className={styles.historia}>
                            <p>
                                Somos una compañía especializada en el transporte terrestre de pasajeros del sur occidente y centro de Colombia, creada el 17 de Marzo de 1956, contamos con más de 63 años de experiencia, tiempo en el que logramos reconocimientos que nos ubican como líderes en el servicio de transporte intermunicipal de pasajeros.
                            </p>
                        </div> */}

                        <div className={styles.cualidades}>
                            <div className={styles.logros}>
                                <p>
                                   {leftDescription} 
                                </p>
                            </div>
                            <div className={styles.info_adicional} dangerouslySetInnerHTML={{ __html: rigthDescription }}>
                            </div>
                            {/* <div className={styles.info_adicional}>
                                <p>
                                    Nos caracteriza la puntualidad, seguridad y comodidad en más de 400.000 despachos anuales, trabajando 24 horas y los 365 días del año, logrando que nuestros pasajeros lleguen a su destino a tiempo.
                                    <br></br>
                                    <br></br>
                                    Contamos con vehículos modernos dotados con equipos de última tecnología permitiendo una mejor experiencia de viaje y calidad en el servicio, los conductores son altamente capacitados en la operación y seguridad de los vehículos, realizamos programas de mantenimientos preventivos que garantizan el óptimo estado de nuestra flota.
                                </p>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>

            {/* <div className={styles.description_ep_movil}>
                <div>
                    <div className={styles.titulo_historia_movil}>
                        <label>Nuestra Historia</label>
                    </div>

                    {topImage ?
                        <div className="d-flex align-items-center" style={{ position: "relative" }}>
                            <div className={styles.line_separator}></div>
                            <Image className={styles.top_image} src={mujeresNosotros} alt="" />
                            <div className={styles.line_separator}></div>
                        </div>
                        : null
                    }

                    <div className={styles.historia_movil}>
                        <p>
                            Somos una compañía especializada en el transporte terrestre de pasajeros del sur occidente y centro de Colombia, creada el 17 de Marzo de 1956, contamos con más de 63 años de experiencia, tiempo en el que logramos reconocimientos que nos ubican como líderes en el servicio de transporte intermunicipal de pasajeros.
                        </p>
                    </div>
                </div>

                <div className="row justify-content-center align-items-center">
                    <div className={styles.movil_bus}>
                        <div className=""><Image className={styles.imgBusNosotros_movil} src={mujeresNosotros} alt="bus-nosotros-movil" /></div>
                    </div>
                    <div className={styles.logros_web}>
                        <p>Hemos desarrollado diferentes unidades de negocios, como son el transporte de carga, en comiendas y mensajería en modo terrestre, fluvial, marítimo y aéreo.</p>
                    </div>
                </div>

                <div className={styles.info_adicional_movil}>
                    <p>
                        Nos caracteriza la puntualidad, seguridad y comodidad en más de 400.000 despachos anuales, trabajando 24 horas y los 365 días del año, logrando que nuestros pasajeros lleguen a su destino a tiempo.
                        <br></br>
                        <br></br>
                        Contamos con vehículos modernos dotados con equipos de última tecnología permitiendo una mejor experiencia de viaje y calidad en el servicio, los conductores son altamente capacitados en la operación y seguridad de los vehículos, realizamos programas de mantenimientos preventivos que garantizan el óptimo estado de nuestra flota.
                    </p>
                </div>

            </div> */}
            {/* <div className={styles.banner_bottom}>
                <div className={styles.banner_bottom_img}>
                    <Image src={mujeresNosotros} alt=""/>
                </div>
                <div className={styles.col_md}>
                </div>
                <div className={styles.descripcion_banner_bottom}>
                    <p>Generamos experiencias de viaje a mas de <span>5 millones</span>  de pasajeros al año, los cuales disfrutan del mejor confort y entretenimiento a bordo.
                    </p>
                </div>

            </div> */}
               
        </div>
    );
}
