import React from "react"
import { InfoSedeProps } from "../../types/basics"
import styles from "./infosede.module.css"

export function InfoSede(props: any ) {
    const { title, sede, images } = props
    const p = sede.phone.toString()
    return (
        <div className={styles.info_sede}>
            {/* {images && <div className="img-sucursal-movil d-block d-md-none">
                <img src={getImageURL(image)} alt="" />
            </div>} */}
            <div className={styles.container_sede_info}>
                <label className={styles.titulo_city}>{title}</label>
                <div className={styles.d_flex}>
                    <label className={styles.titulo_attribute}>Ubicación:</label>
                    <p className={styles.description_sede_text}>{sede.location}</p>
                </div>
                <div className="">
                    <label className={styles.titulo_attribute}>Dirección:</label>
                    {sede.address.split("/n").map((item: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined , index: React.Key | null | undefined) =>
                        <p key={index} className={styles.description_sede_text}>{item}</p>
                    )}
                </div>
                <div className={styles.d_flex}>
                    <label className={styles.titulo_attribute}>Celular:</label>
                    <p className={styles.description_sede_text}>{`${p.slice(0,3)} ${p.slice(3,6)} ${p.slice(6,9)} ${p.slice(9)}`}</p>
                </div>
                <div className="">
                    <label className={styles.titulo_attribute}>Horario de Atención:</label>
                    <p className={styles.description_sede_text}>{sede.officeHours}</p>
                </div>
            </div>
            {/* {images && <div className="img-sucursal d-none d-md-block">
                <img src={getImageURL(image)} alt="" />
            </div>} */}
        </div>
    )
}