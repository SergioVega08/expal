/* eslint-disable @next/next/link-passhref */
import React from "react"
import styles from "./inputSearch.module.css"
import lupa from "../../img/lupa-blanca.svg"
import Link from 'next/link'
import { withInstantSearch } from "next-instantsearch";
import {
    RefinementList,
    SearchBox,
    Hits,
    Configure,
    Highlight,
    Pagination,
    InstantSearch,
} from 'react-instantsearch-dom';
// import { JSONApiBody, FaqQuestion } from "../../../types/"
import { algoliaSearchClient } from "../../context/algolia"
import { SearchInput } from "../searchInput"

const Question = (props: { hit: any }) => {
    const { id, attributes: hit } = props.hit
    // console.log(props.hit)
    return (
        <div>
            <Link href={`/servicio-cliente/preguntas?id=${id}`}>hit.question.value
                <p className="faq-question">{hit.question.value}</p>
            </Link>
        </div>
    )
}

// export interface InputSearchProps {
// }

export interface InputSearchModel {
    description: string
    popularQuestions: {
        title: string
    }
}

export function InputSearch(props: any) {

    const popular: InputSearchModel = {
        description: "Resuelve todas las dudas relacionadas con tu viaje navegando por nuestras preguntas frecuentes",
        popularQuestions: {
            title: "Preguntas frecuentes",
        }

    }
    return (
        <div className={`d-flex ${styles['input-search']} justify-content-between`}>
            <div className={styles['inputs-quiestions']}>
                <div className={styles['search-description']}>
                    <p>{popular.description} </p>
                </div>

                <SearchInput />
            </div>

            <div className={`${styles['question-list']} d-none d-md-block`}>
                <div className={styles['title-popular']}>
                    <label htmlFor="">{popular.popularQuestions.title}</label>
                </div>

                <div className={styles['popular-questions']}>
                    <InstantSearch
                        searchClient={algoliaSearchClient}
                        indexName="questions"
                    >
                        <Configure
                            // filters="free_shipping:true"
                            hitsPerPage={4}
                            // analytics={false}
                            // enablePersonalization={true}
                            distinct
                        />
                        <Hits hitComponent={Question} />
                    </InstantSearch>
                </div>
            </div>
        </div>
    )
}


