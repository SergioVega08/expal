/* eslint-disable @next/next/no-img-element */
import React from "react"
import ministerio from "../../img/footer_logo_1.jpg"
import ani from "../../img/footer_logo_2.jpg"
import invias from "../../img/footer_logo_3.jpg"
import st from "../../img/footer_logo_4.jpg"
import styles from "./insidefootericons.module.css"

// export interface InsideMediosdPagoProps {
//     data: JSONApiBody<MedioPago>[]
//     origin: any
// }

export function FooterLinks() {
    // if (!origin) return null

    const Images = [
       {
        name:"Ministerio",
        src:ministerio.src,
        url:"https://www.mintransporte.gov.co/"
       },

       {
        name:"ANI",
        src:ani.src,
        url:"https://www.ani.gov.co/"
       },
       {
        name:"Invias",
        src:invias.src,
        url:"https://www.invias.gov.co/"
       },

       {
        name:"ST",
        src:st.src,
        url:"https://supertransporte.gov.co/"
       }
    ]

    return (
        <div className={styles.inside_medios_pago}>
            <div className={styles.link_logo_pago}>
                {Images.map((item, index) => {
                    // const image: JSONApiBody<Image> = item.relationships.image.data && origin.files[item.relationships.image.data.id]
                    return (<a key={index} className="" href={item.url}>
                        <img className={styles.logo_pago} alt="" src={item.src} />
                    </a>)
                }
                )}
            </div>
        </div>
    )
}