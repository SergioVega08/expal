/* eslint-disable @next/next/no-img-element */
import React from "react"
import americanEx from "../../img/american_express.png"
import baloto from "../../img/baloto.png"
import efecty from "../../img/efecty.png"
import pse from "../../img/pse.png"
import visa from "../../img/visa.png"
import masterCard from "../../img/mastercard.png"
import styles from "./insidemediosdepago.module.css"

// export interface InsideMediosdPagoProps {
//     data: JSONApiBody<MedioPago>[]
//     origin: any
// }

export function IconsLinks() {
    // if (!origin) return null

    const Images = [
       {
        name:"American Express",
        src:americanEx.src,
        url:"https://www.americanexpress.com/"
       },

       {
        name:"Baloto",
        src:baloto.src,
        url:"https://www.baloto.com/"
       },
       {
        name:"Efecty",
        src:efecty.src,
        url:"https://www.efecty.com.co"
       },

       {
        name:"PSE",
        src:pse.src,
        url:"https://www.pse.com.co/persona"
       },

       {
        name:"Visa",
        src:visa.src,
        url:"https://www.visa.com.co"
       },

       {
        name:"Master Card",
        src:masterCard.src,
        url:"https://www.mastercard.com.co/es-co.html"
       },
    ]

    return (
        <div className={styles.inside_medios_pago}>
            <div className={styles.link_logo_pago}>
                {Images.map((item, index) => {
                    // const image: JSONApiBody<Image> = item.relationships.image.data && origin.files[item.relationships.image.data.id]
                    return (<a key={index} className="" href={item.url}>
                        <img className={styles.logo_pago} alt="" src={item.src} />
                    </a>)
                }
                )}
            </div>
        </div>
    )
}