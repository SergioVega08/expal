import React, { ReactNode } from "react"
import style from "./insideserviceclient.module.css"

import { LeftSearchPanel } from "../leftsearchpanel"



export interface InsideServiceClientProps {
    contentLayout?(): ReactNode
}

export function InsideServiceClient(props: InsideServiceClientProps) {
    const { contentLayout } = props
    return (
        <div className="d-flex">
            <div className={style['content-layout']}>
                {contentLayout()}
            </div>
            <div className={style['left-search-panel']}>
                <LeftSearchPanel />
            </div>
        </div>
    )
}