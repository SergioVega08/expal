import React from "react";
import Image from "next/image";
import arrow from "../../img/flecha_adelante.svg";
import { LabelProps } from "../../types/basics";
import Link from "next/link";
import styles from "./labelindicator.module.css";

export function Labelindicator(props: LabelProps) {
    const {
        routes,
        // style,
        className: classOverride
    } = props

    return (
        <div className={styles.label_content}>
            <div  className={styles.label_indicator} >
                {routes.map((item, index) => {

                    if (index !== routes.length - 1) {
                        return (<>
                            <div >
                                <label className={styles.label_green}>
                                    <Link href={item.url} >
                                        {item.label}
                                    </Link>
                                </label>
                            </div>
                            <div className={styles.label_arrow}>
                                <Image src={arrow} width={20} height={20} alt="" />
                            </div>
                        </>)
                    } else {
                        return (
                            <div className={styles.label_indicator}>
                                <div >
                                    <label>
                                        {/* <Link to={item.url}> */}
                                        {item.label}
                                        {/* </Link> */}
                                    </label>
                                </div>
                            </div>
                        )
                    }
                })}
            </ div>
        </div>
    );
}