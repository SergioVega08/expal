/* eslint-disable jsx-a11y/alt-text */
import React from "react"
import styles from "./leftsearchpanel.module.css"
import lupa from "../../img/lupa-blanca.svg"
import tell from "../../img/telefono-verde.svg"
import fijo from "../../img/telefono-fijo.svg"
import cell from "../../img/celular-verde.svg";
import whatt from "../../img/whastapp-verde.svg";
import envioDocu from "../../img/envio-documento-verde.svg"
import dirconcatto from "../../img/direccion_contacto-verde.svg"
import Link from 'next/link'
import Image from 'next/image'

import { SearchInput } from "../searchInput";

export function LeftSearchPanel() {
    return (
        <div className={styles['left-search-panel-font']}>
            <div className={styles['background-tittle']}>
                <h4>
                    ¡Si tienes pregúntas sobre el <span>viaje,</span> pregúntanos!
                </h4>
            </div>
            <SearchInput preset="compact" />
            <div className="">
                <a href="tel:018000936662" className="margin-vertical">
                    <div className={`d-flex justify-content-around align-items-center ${styles['comunication-medium']}`}>
                        <div className={styles.linImg}>
                            <Image src={tell} />
                        </div>
                        <div className={styles.lintxt}>
                            <label>Línea Nacional: <br></br>01 8000 93 66 62</label>
                        </div>
                    </div>
                </a>

                <a href="tel:3110640" className={styles['margin-vertical']}>
                    <div className={`d-flex justify-content-around ${styles['comunication-medium']}`}>
                        <div className={styles.celimg}><Image src={tell} /> </div>
                        <div className={styles.celtxt}><label>Línea Fija:<br></br>(+2) 3 110 640</label> </div>
                    </div>
                </a>

                {/* <a href="tel:3112730419" className="margin-vertical">
                    <div className="d-flex justify-content-around comunication-medium">
                        <div className="celimg"><img src={cell}></img> </div>
                        <div className="celtxt"><label>Celular:<br></br>(+57) 311 273 04 19</label> </div>
                    </div>
                </a> */}
                {/* <div className="margin-vertical">
                    <a href="https://wa.me/573105452791?text=Hola%2C%20quiero%20hacer%20una%20pregunta%3A" className="d-flex justify-content-around comunication-medium">
                        <div className="whatsapp-img"><img src={whatt}></img> </div>
                        <div className="whatsapp-txt"><label>WhatsApp:<br></br>(+57) 310 545 27 91</label></div>
                    </a>
                </div> */}

                <a href="https://www.expresopalmira.net/transporte/pqr.php" className={styles['margin-vertical']}>
                    <div className={`d-flex justify-content-around ${styles['comunication-medium']} ${styles['padding-aditional']}`}>
                        <div className={styles['whatsapp-img']}><Image src={envioDocu} /> </div>
                        <div className={styles['whatsapp-txt left-margin']}><label>Fomulario de contacto</label></div>
                    </div>
                </a>

                <div className="margin-vertical">
                    <div className={`d-flex justify-content-around ${styles['comunication-medium']} ${styles['padding-aditional']} ${styles['no-border']} no-border`}>
                        {/* <Link href="/sedes"> */}
                        <div className={styles['whatsapp-img']}><Image src={dirconcatto} /> </div>
                        <div className={styles['whatsapp-txt left-margin']}><label>Oficinas de Atención</label></div>
                        {/* </Link> */}
                    </div>
                </div>
            </div>
        </div>
    )
}