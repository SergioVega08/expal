import React, { useState, ReactNode, Children, useRef, useEffect } from "react"
import styles from "./listdropdown.module.css"
import dirAbajo from "../../img/flecha-abajo.svg"
import dirArriba from "../../img/flecha_arriba.svg"

const scrollToRef = (ref: any) => window && window.scrollTo(0, ref.current.offsetTop - 100)

export interface CardType<T> { key?: string, label: string, value: T }

export interface ListDropdpwnProps<T> {
    renderItem?(props: { item: T, index: number }): ReactNode
    data: CardType<T>[]
}

export function ListDropdown<T>(props: ListDropdpwnProps<T>) {
    const { data, renderItem } = props
    if (typeof window == 'undefined') return null
    const urlParams = new URLSearchParams(window.location.search)
    const id = urlParams.get('id')
    const cardRef = React.createRef()
    setTimeout(() => scrollToRef(cardRef), 1000)

    const [cardIndex, setcardIndex] = useState(-1)

    return (
        <div className={styles.card_list}>
            {data.map((item, index) =>
                <CardListItem
                    key={index}
                    index={index}
                    ref={item.key && id == item.key ? cardRef : React.createRef()}
                    renderItem={renderItem}
                    active={cardIndex == -1 ? (item.key ? id == item.key : false) : cardIndex == index}
                    data={item}
                    onOpen={index => setcardIndex(index)}
                />)}
        </div>
    )

}

export interface CardListItemProps<T> {
    active?: boolean
    data: CardType<T>
    index: number
    renderItem?(props: { item: T, index: number }): ReactNode
    onOpen?: (index: number) => void
    onClose?: (index: number) => void

}

export const CardListItem = React.forwardRef(function <T> (props: CardListItemProps<T>, ref: any){
    const defaultRenderItem = ({ item, index }: { item: string[], index: number }) =>
        <div >
            {item.length == 1 ?
                <div className={styles.listdropdown_style} dangerouslySetInnerHTML={{ __html: item[0] }}></div> //className="listdropdown-style"
                :
                (item.map((elem, index) =>
                    <div key={index} className={styles.list_elements}>
                        <li className="" dangerouslySetInnerHTML={{ __html: elem }}></li>
                    </div>
                ))}
        </div>


    const { renderItem = defaultRenderItem, data, index, onOpen, onClose, active = false } = props
    const { label, value, key } = data

    useEffect(() => {
        setShow(active)
    }, [active])

    const [show, setShow] = useState(active)
    const showButton = (e: any) => { onOpen && onOpen(index); setShow(true) }
    const hideButton = (e: any) => { onClose && onClose(index); setShow(false) }

    return (
        <div >
            <div className={styles.box_label}>
                <div>
                    <h1 ref={ref} id={key} className={styles.label}>{label}</h1>
                </div>
                {show ?
                    <div onClick={hideButton}>
                        <img src={dirArriba} alt="" />
                    </div>
                    :
                    <div onClick={showButton}>
                        <img src={dirAbajo} alt="" />
                    </div>
                }
            </div>
            {show &&
                <div >
                    {renderItem({ item: value as any, index })}
                </div>
            }
        </div>

    )
    
}