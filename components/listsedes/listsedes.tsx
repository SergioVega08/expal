import React from "react"
import { ListSedesProps } from "../../types/basics"
import styles from "./listsedes.module.css"

// export interface ListSedesProps {
//     sucursales: string[]
//     onCity: (index: string) => void
// }

export function ListSedes(props: ListSedesProps) {
    const { sucursales, onCity } = props
    return (
        <div className={styles.list_sedes}>
            <div className={styles.list_sedes_container}>
                <ul className={styles.list_sedes}>
                    {sucursales.map((city, index) =>
                        <div key={index} className={styles.li_container}>
                            <li>
                                <a className={styles.cityLink} onClick={() => onCity(city)}>{city}</a>
                            </li>
                        </div>
                    )}
                </ul>
            </div>
        </div>
    )
}