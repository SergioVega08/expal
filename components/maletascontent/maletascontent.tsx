import React from "react"
import style from "./maletascontent.module.css"
import { MaletasDescriptions } from "../maletasdescriptions"
// import { JSONApiBody, Page, ItemPromotion } from "../../../types/"



export function MaletasContent({ data }: any) {
    const sections = data.body.split('<hr />')

    const maletasItem = {
        'nodeItemPromotion': [{
            'attributes': {
                'title': 'En Bodega',
                'subTitle': '25 Kilos',
                'quantity': '1',
                'backgroundColor': { 'color': 'rgb(255, 233, 0)' },
                'itemDescription': ['Una maleta grande máximo de 25 kg.', 'La medida no debe superar los 80cm x 50cm x 30cm, esta maleta viaja en bodega.']
            }
        },
        {
            'attributes': {
                'title': 'De mano',
                'subTitle': '10 Kilos',
                'quantity': '1',
                'backgroundColor': { 'color': 'rgb(57, 199, 47)' },
                'itemDescription': ['Una maleta de mano máximo de 10 kg.', 'Medida no debe superar los 40cm x 30cm x 20cm.']
            }
        },
        ]
    }
    return (
        <div className={style['container-maletas']}>
            <div className={style['content-maletas']} dangerouslySetInnerHTML={{ __html: sections[0] }}></div>
            <div className="">
                <MaletasDescriptions data={maletasItem} />
            </div>
            <div className={style['content-maletas']} dangerouslySetInnerHTML={{ __html: sections[1] && sections[1] }}></div>

        </div>
    )
}
