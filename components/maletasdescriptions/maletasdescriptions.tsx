import React from "react"
import style from "./maletasdescriptions.module.css"
import maletabodega from "../../img/maletin-claro.svg"
import maletamano from "../../img/maleta-clara.svg"
import cerrar from "../../img/cerrar-claro.svg"
import Image from 'next/image'

// import { ItemPromotion, JSONApi, JSONApiBody, Image } from "../../../types/"


export function MaletasDescriptions({ data }: any) {

    const maletas: any[] = Object.values(data.nodeItemPromotion)

    return (
        <div className={style['maletadescription-container']}>
            {maletas.map((item, index) => {
                console.log(item);
                const { title, subTitle, quantity, backgroundColor, itemDescription } = item.attributes
                // const image: any = item.relationships.image.data && data.files[item.relationships.image.data.id]
                // console.log(data.files)
                return (
                    <div key={index} className={`d-flex ${style['container-maleta']} align-items-center justify-content-center`}>
                        <div className={style['icons-maleta']}>
                            <div className={style['tittle-maleta']}>
                                <label>{title}</label>
                            </div>
                            <div className={`${style['container-img-maleta']} d-flex align-items-center`}>
                                <img className={style['img-maleta']} src={''} alt="" />
                                <div className={style['img-x']}>
                                    <Image src={cerrar} alt="" />
                                </div>
                                <label>{quantity}</label>
                            </div>
                            <label className={style['peso-maleta']}>{subTitle}</label>
                        </div>
                        <div className={style['backgroun-maleta']} style={{ backgroundColor: backgroundColor.color }}></div>
                        <ul className={style['descriptions-maleta']}>
                            {itemDescription.map((d: any, index: number) =>
                                <li key={index}>{d}</li>
                            )}
                        </ul>
                    </div>
                )
            }
            )}
        </div>
    )
}