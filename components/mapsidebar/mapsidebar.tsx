import React from "react"
import styles from "./mapsidebar.module.css"
import mapa from "../../img/rutas_mapa-01.svg"
import Image from 'next/image'

export function Mapsidebar(props: any) {
    return (
        <div className={styles['map-container']}>
            <div className={styles.absoluteDiv}>
                <Image className={styles['map-img']} src={mapa} />
            </div>
        </div>
    );
}