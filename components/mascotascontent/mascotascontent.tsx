import React from "react"
import style from "./mascotascontent.module.css"

export interface MascotasContentProps {
    data: any
}

export function MascotasContent({ data }: MascotasContentProps) {
    return (
        <div className={style['mascotas-content']} dangerouslySetInnerHTML={{ __html: data.body }}>


        </div>

    )
}