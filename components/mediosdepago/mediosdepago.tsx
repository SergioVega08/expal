import React from "react";
import { IconsLinks } from "../insidemediosdepago/insidemediosdepago";
import styles from "./mediosdepago.module.css"

export function Mediosdepago() {
    return (
        <div className={styles.mediosPagoContainer}>
            <div className={styles.fontw_strong}>
                <div className={styles.subitem_1}>
                    <label className={styles.margin_top}>Paga fácil, rápido y seguro</label>
                    <div className={styles.container_text}>
                        <p className={styles.justify_right}>desde cualquier lugar.</p>
                    </div>
                </div>
                <p className={styles.size}>Paga fácil, rápido y seguro desde cualquier lugar.</p>
            </div>

            <div className={styles.right_side_logos}>
                <div className={styles.titulo }>
                    <label>Medios de pago disponible:</label>
                </div>
                <div className={styles.logos}>
                    <IconsLinks />
                </div>
            </div>

        </div>
    );
}