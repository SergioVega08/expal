import React from "react"
import style from "./menorescontent.module.css"

export function MenoresContent({ data }: any) {
    return (
        <div>
            <div className={style.InnerCode} dangerouslySetInnerHTML={{ __html: data.body }}></div>
        </div>
    )
}