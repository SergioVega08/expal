/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/jsx-key */
import React, { useState, useEffect } from "react"
// import { Link, Router } from '@reach/router'
import styles from './navMenu.module.css'
import usuarioblanco from "../../img/usuario-blanco.png";
import burgerclose from "../../img/cerrar-verde.png"
import tell from "../../img/telefono-verde.png"
import fijo from "../../img/telefono-fijo.svg"
import cell from "../../img/celular-verde.svg";
import whatt from "../../img/whastapp-verde.svg";
import { link } from "fs";
import Link from "next/link";
import { isNull } from "lodash";

export function NavMenu(props: any) {
    const { show } = props
    const [status, setStatus] = useState(show)

    useEffect(() => {
        if (show) setStatus(show)
    }, [show])

const NavMenu = ({isDropDown, title,childrens, route }: { isDropDown :boolean, title: string, childrens:any, route: string}) => {
        // console.log(menu)
        return (
        <>
            {isDropDown ?
                <>
                <ul>
                    <li className={styles['tittle-list']}>
                        <a className={styles['tittle-list']}>{title}</a>
                        <div className={styles.navLabel}>
                        {childrens.map((item: any, index: number) => {
                            return(
                            <div className ={`margin ${styles['tittle-list-li']}`}> 
                                <a key={index} href={item.route}>{item.name}</a><br></br>
                            </div>)
                        })}
                        </div>
                    </li>
                    
                </ul>
                </>
                : <>
                    <ul> 
                    <li className={styles['title-list']}>
                        <a href={route}>{title}</a>
                        </li>
                    </ul>
                 </> 
             }       
        </>
        
    )
}
    return (
        status ?
            <div className= {`d-xs-block d-md-none  flex-column ${styles['navmenu']}`}>

                <div className={`${styles['navigation-items']} ${styles['side-draw']} d-flex flex-column justify-content-start`}>
                    <div className={`${styles['close-navmenu']} align-self-end`}><button className={styles['close-buttom']} onClick={() => setStatus(false)}><img src={burgerclose.src} alt="" className={styles['img-close'] }/></button></div>
                    <div className={`${styles['iniciar-sesion']} d-flex align-items-center margin ${styles['button-margin']} align-self-start`}><img src={usuarioblanco.src} alt="" /><label>Iniciar Sesión</label></div>
                    {/* <ul className="links-responsive d-flex flex-column margin margin-lg">
                        {menu.map((item) =>
                            <li className="margin"> <Link to={item.path}> {item.label.toUpperCase()} </Link> </li>
                        )}
                    </ul> */}
                    <div className={styles['links-responsive']}>

                        {/* {titulosHeader.map((item, index) =>
                            <NavMenu title={item.titulo} object={columnHeader1}/>
                        )} */}
                        <NavMenu isDropDown = {false} title={"INICIO"} childrens ={null} route="/"/>
                        <NavMenu isDropDown = {true} title={"NOSOTROS"}  route="" childrens ={
                            [
                                {name: "Nuestra Historia",
                                 route: "/nosotros",
                                },
                                {name: "Fundación",
                                route: "/fundacion",
                                },
                                {name: "Sedes",
                                route: "/sedes",
                                },
                            ]
                        }/>
                        <NavMenu isDropDown = {false} title={"RUTAS"} childrens ={null} route="/rutas"/>
                        <NavMenu isDropDown = {true} title={"BUSES"}  route="" childrens ={
                            [
                                {name: "S26",
                                route: "/buses",
                                },
                                {name: "METTRO",
                                route: "/buses",
                                },
                                {name: "METTRO X",
                                route: "/buses",
                                },
                                {name: "S26 MAXXI DUPPLO",
                                route: "/buses",
                                },
                            ]
                        }/>
                        <NavMenu isDropDown = {false} title={"SERVCIO AL CLIENTE"} childrens ={null} route="/servicio-al-cliente"/>
                        <NavMenu isDropDown = {false} title={"PQRS"} childrens ={null} route="https://www.expresopalmira.net/transporte/pqr.php"/>

                    </div>
                    <div className={`${styles['line-divider']} align-self-center`}></div>

                    <div className={styles['contact-responsive']}>
                        <a href="tel:018000936662" className={`d-flex justify-content-left ${styles['marginMenu']}`}>
                            <div className={`linImg ${styles['margin-sides']}`}>
                                <img src={tell.src}></img>
                            </div>
                            <div className={`lintxt ${styles['margin-sides']}`}>
                                <label>Línea Nacional: <br></br>01 8000 93 66 62</label>
                            </div>
                        </a>
                        
                        <a href="tel:3110640" className={`d-flex justify-content-left ${styles['marginMenu']}`}>
                            <div className={`celimg ${styles['margin-sides']}`}><img src={tell.src}></img> </div>
                            <div className={`celtxt ${styles['margin-sides']}`}><label>Línea Fija:<br></br>(+2) 3 110 640</label> </div>
                        </a>

                    </div>

                    <div className="line-divider align-self-center"></div>

                    <div className={`${styles['info-responsive']} margin-lg`}>
                        <NavMenu isDropDown = {true} title={"EXPRESO PALMIRA"} route="" childrens={[
                            {
                                name: "Sobre Expreso Palmira",
                                route: "/nosotros",
                            },
                            {
                                name: "Cliente Ultra",
                                route: "/ultra",
                            },
                            {
                                name: "Sedes",
                                route: "/sedes",
                            },
                            {
                                name: "Términos y Condiciones",
                                route: "/servicio-cliente/politica",
                            },
                            {
                                name: "PQRS",
                                route: "https://www.expresopalmira.net/transporte/pqr.php",
                            },
                        ]}/>

                    </div>

                    <div className={`${styles['line-divider']} align-self-center`}></div>

                    <div className={`${styles['beforetravel']} margin-lg`}>
                         <NavMenu isDropDown = {true} title={"ANTES DE VIAJAR"} route="" childrens={[
                            {
                                name: "Contrato de transporte",
                                route: "/servicio-cliente/contrato",
                            },
                            {
                                name: "Preguntas Frecuentes",
                                route: "/servicio-cliente/preguntas",
                            },
                            {
                                name: "Viaje con menores",
                                route: "/servicio-cliente/menores",
                            },
                            {
                                name: "Transporte de mascotas",
                                route: "/servicio-cliente/mascotas",
                            },
                            {
                                name: "Tiquetes en Línea",
                                route: "/servicio-cliente/onlinetiquets",
                            },
                            {
                                name: "Manejo de equipaje",
                                route: "/servicio-cliente/equipaje",
                            },
                        ]}/>
                    </div>

                    <div className="line-divider align-self-center"></div>

                    <div className="destinos-rutas margin-lg last-margin">
                    <NavMenu isDropDown = {true} title={"DESTINOS Y RUTAS"} route="" childrens={[
                           {
                            name: "Viajar a Bogotá",
                            routes: "/rutas",
                            }
                        ]}/>

                    </div>
                </div>
                <div className={styles['backdrop']} onClick={() => setStatus(false)}></div>
            </div>
            : null
    )
}