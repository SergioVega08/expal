import React from "react"
import style from "./onlinecontent.module.css"
import { ListDropdown } from "../listdropdownBeta"
import xpath from 'xpath'
import { DOMParser as dom } from 'xmldom'

const tittle = `<h3>Tiquetes en línea</h3>`

export interface ListModel {
    items: { label: string, value: string[] }[]
}

export function OnlineContent({ data }: any) {

    const html = data.body
    
    const doc = new dom().parseFromString(html);
    const labels = xpath.select("//h3", doc)
    const values = xpath.select("//h3/following-sibling::*[1]", doc)

    const listDropdown = labels.map((label: any, index) => {
        const value = values[index]
        return label ? { label: label.firstChild.data, value: [value.toString()] } : null
    }).filter(v => !!v)

    return (
        <div className={style['online-content-container']}>
            <div className={style['online-content']} dangerouslySetInnerHTML={{ __html: tittle }}></div>
            <div className={style['value-menu']}>
                <ListDropdown
                    data={listDropdown}
                />
            </div>

        </div>
    )
}