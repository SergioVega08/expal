import styles from "./rutas.module.css";
import React, { useMemo, CSSProperties, useEffect, useContext } from "react";
import { RutasTable } from "../rutasTable/rutasTable";
import { routesTable } from "../../utils/json";
// import { Route, JSONApiBody } from "../../../types/";
//import Bannerg from "../componenteGenerales/js/bannerg";
// import {scrollTop} from "../../utils"

const presets = {
    default: {},
    compact: {},
}

type RutasPresets = keyof typeof presets

// export interface RutasProps {
//     title: string
//     routes: JSONApiBody<Route>[]
//     preset?: RutasPresets
//     className?: string
//     style?: CSSProperties
//     columns?: string[]
// }

export function Rutas(props: any) {
    const context = useContext
    const { routes, title, className, columns, style, preset = 'default' } = props

    // const routesData = useMemo(() => routesTable, [])
    const routesData = routesTable.map((item: any) => {
        return {
            'attributes': {
                'src': item.src,
                'dst': item.dst,
                'leaveHour': item.leaveHour,
                'price': item.price
            }
        }
    });
    // useEffect(() => {
    //     scrollTop()
    // }, [])

    return (
        <div className={`${styles.rutas} ${className}`} style={style}>
            <div className={styles['title-disponibles']}>
                <label> {title} </label>
            </div>
            <RutasTable
                filterCols={columns}
                data={routesData}
                preset={preset}
            />

            {/* <div>
                <button className="search-buttom d-block d-md-none">Buscar</button>
            </div> */}

            <div className={styles['info-rutas']}></div>
        </div>
    );
}