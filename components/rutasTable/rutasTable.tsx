import React, { useMemo, useState, useContext } from 'react'
import { useTable, useFilters, Column } from 'react-table'
import style from "./rutasTable.module.css"
import { getFormatedCurrency, getFormattedTime } from '../../utils/text'
// import { Route, JSONApiBody } from '../../../types/'
// import { BoxTicket } from 'components/boxticket'
import * as _ from 'lodash'
import { SelectColumnFilter } from '.'
import { CSSProperties } from 'react'
import { RutasContext } from '../../context/routesCntext'

const presets = {
  default: {},
  compact: {},
}

type RutasTablePresets = keyof typeof presets

// export interface RutasTableProps {
//   filterCols?: string[]
//   data: JSONApiBody<Route>[]
//   preset?: RutasTablePresets
// }

export function RutasTable(props: any) {
  const { filterCols, data, preset = 'default' } = props

  const columns = useMemo(
    () => [
      {
        Header: 'Origen',
        accessor: 'src',
        Filter: SelectColumnFilter,
        filter: 'equals'
      },
      {
        Header: 'Destino',
        accessor: 'dst',
        Filter: SelectColumnFilter,
        filter: 'equals'
      },
      {
        Header: 'Horario',
        accessor: 'leaveHour',
        Filter: SelectColumnFilter,
        Cell: ({ cell: { value } }) => getFormattedTime(value)
      },
      {
        Header: 'Precio',
        accessor: 'price',
        Cell: ({ cell: { value } }) => getFormatedCurrency(value)
      },
      // {
      //   Header: 'Bus',
      //   accessor: 'buses',
      //   Filter: SelectColumnFilter,
      //   filter: 'equals'
      // },
    ] as Column<object>[],
    []
  )

  const defaultColumn = useMemo(
    () => ({
      Filter: SelectColumnFilter,
    } as any),
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
  } = useTable(
    {
      columns,
      data: data.map((x: any) => x.attributes),
      defaultColumn
      // filterTypes,
    },
    useFilters
  )

  const state1: any = state
  // const firstPageRows = rows.slice(0, 10)

  const { routeSelected, setrouteSelected } = useContext(RutasContext)
  const getRowProps = (row: any) => ({
    onClick: () => {
      // console.log(row)
      setrouteSelected(row)
    },
    style: {
      ...(row.index == (routeSelected != null) ? routeSelected.index : '') && { color: 'green' },
      cursor: 'pointer',
    } as CSSProperties
  })

  return (
    <>
      {/* <div>
        <pre>
          <code>{JSON.stringify(state1.filters, null, 2)}</code>
        </pre>
      </div> */}

      {headerGroups.map((headerGroup, index) =>
        <div key={index} className={`${style['options']} d-md-flex`}>
          {headerGroup.headers.filter((column: any) => filterCols ? filterCols.includes(column.id) : true).map((column: any, index) => {
            return (
              <div key={index} className={`${style['ruta-field']} d-flex align-items-start ${preset === 'compact' ? 'flex-column' : 'flex-column flex-md-row'}`}>
                <div className={style['title-field']}>
                  <label>{column.render('Header')}</label>
                </div>
                <div className={style.selectors}>
                  <div className="">{column.canFilter ? column.render('Filter') : null}</div>
                </div>
              </div>)
          })}
        </div>
      )}
      <div className={`${style['routeTableContainer']} table-responsive`}>

        <table {...getTableProps()} cellSpacing="0" cellPadding="0" className={`${style['routeTable']} table table-striped`}>
          <thead>
            {headerGroups.map((headerGroup, index: number) => (
              <tr key={index} {...headerGroup.getHeaderGroupProps()}>
                {
                  headerGroup.headers.map((column: any, index) => (
                    <th key={index} scope="col" {...column.getHeaderProps()}>
                      {column.render('Header')}
                    </th>
                  ))
                }
              </tr>
            ))}
          </thead>
          {/* <div className="rutasTableBody"> */}
          <tbody {...getTableBodyProps()}>
            {rows.map(
              (row, i) => {
                prepareRow(row);
                return (
                  <tr {...{ ...row.getRowProps(), ...getRowProps(row) }}>
                    {row.cells.map(cell => {
                      return (
                        <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                      )
                    })}
                  </tr>
                )
              }
            )}
          </tbody>
          {/* </div> */}
        </table>

      </div>
      <div className="d-md-none d-block ticketContainer">
        {_.filter(data, { attributes: state1.filters }).map((tiquet, index) => { }
          // <BoxTicket key={index} route={tiquet} withDropdown />
        )}
      </div>
    </>
  )
}
