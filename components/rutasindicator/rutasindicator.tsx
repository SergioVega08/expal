/* eslint-disable @next/next/no-img-element */
import React from "react"
import busruta from "../../img/bus_sedes-verde.svg"
import { Route } from "../../types/basics"
import styles from "./rutasindicator.module.css"

export interface RutasIndicatorProps{
    routes: Route[]
}

export function RutasIndicator(props: any) {
    const {routes} = props

    return (
        <div className={styles.rutas_indicator}>

            <div className={styles.title_avaible}>
                <label>Rutas</label>
                <label>disponibles:</label>
            </div>

            <div className={styles.container_rutas_movil}>
                <div className={styles.separator_box}></div>

                <div className={styles.separator_box_movil}></div>
                <div className={styles.container_rutas}>
                    <div className={styles.container_rutas_indicators}>
                        {routes.length > 0 ? routes.map((ruta: { src: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; dst: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined }, index: React.Key | null | undefined) =>
                            <div key={index} className={styles.box_ruta}>
                                <img className={styles.bus_rutas}src={busruta} alt="" />
                                <label className={styles.org_dst_txt}>{ruta.src}</label>
                                <div className={styles.horizontal_separator}></div>
                                <label className={styles.org_dst_txt}>{ruta.dst}</label>
                            </div>
                        ) : <label className={styles.org_dst_txt}>Ninguna ruta por el momento.</label>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}