/* eslint-disable react/jsx-key */
import React, { ReactNode } from 'react'
import Link from 'next/link'
import { InstantSearch, Configure } from 'react-instantsearch-dom';
// import { JSONApiBody, FaqQuestion } from '../../../types/'
import { algoliaSearchClient } from '../../context/algolia'
import styles from './searchInput.module.css'
import { connectSearchBox, connectStateResults, connectHits } from 'react-instantsearch-dom';
import lupa from "../../img/lupa-blanca.svg"
import navigate from "next/link"
import Image from 'next/image'

const Question = (props: { hit: any }) => {
    const { id, attributes: hit } = props.hit
    return (
        <div>
            <Link href={`/servicio-cliente/preguntas?id=${id}`} >hit.question.value
                <p>{hit.question.value}</p>
            </Link>
        </div>
    )
}

const Content = connectStateResults((props: any) => {
    const { searchState, searchResults } = props
    // console.log(props)
    return (searchResults && searchResults.nbHits !== 0 && searchResults.query !== '' &&
        <div className={styles['results']}>
            {searchResults.hits.map((hit: any) => (
                <Question hit={hit as unknown as any} />
            ))}
        </div>
    )
})

const presets = {
    default: {},
    compact: {}
}


export type SearchInputPresets = keyof typeof presets

export interface SearchInputprops {
    submitComponent?(): ReactNode
    preset?: SearchInputPresets
}

export const SearchInput = (props: SearchInputprops) => {
    const { submitComponent, preset = "default" } = props

    const DefaultSearchBox = connectSearchBox(({ currentRefinement, isSearchStalled, refine }) => (
        <form noValidate action="" role="search" className={`d-flex ${styles['search-box']}`}>
            <input
                className="form-control"
                type="text"
                value={currentRefinement}
                onChange={event => refine(event.currentTarget.value)}
                placeholder="Que pregunta tienes?"
            />
            <button className={preset == 'default' ? `d-flex ${styles['search-question']} justify-content-around align-items-center` : `${styles['inside-buttom']}`}>
                <div className={styles.imgLup}><Image src={lupa} alt="" /></div>
                {preset == 'default' && <label>Buscar</label>}
            </button>
            {isSearchStalled ? 'My search is stalled' : ''}
        </form>
    ))

    return (
        <div className={`${styles['search-input']}-field preset-${preset}`}>
            <InstantSearch
                searchClient={algoliaSearchClient}
                indexName="questions"
            >
                <Configure
                    // filters="free_shipping:true"
                    hitsPerPage={4}
                    // analytics={false}
                    // enablePersonalization={true}
                    distinct
                />
                <DefaultSearchBox />
                <div className={styles['results-container']}>
                    <Content />
                </div>
            </InstantSearch>
        </div >
    )
}