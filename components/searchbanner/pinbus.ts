const pinbusURL = 'https://fics.expal.com.co/palm/FICS.B2C/MainForm.aspx'

export interface CityType {
    city: { name: string, id: string, code: string }
}
export interface City2Type {
   id: number, name: string 
}

export interface StationsType {
    status: string
    token: string
    content: CityType[]
}
export interface TerminalType {
  content: City2Type[]
}

export function getRedirectURL(data: { from: City2Type, to: City2Type, fromDate: Date, toDate?: Date, passengers: number }): string {
    const { from, to, fromDate, toDate, passengers } = data

    const salida = fromDate.toISOString().split('T')[0].split("-");
    let returnDate = "";
    if(toDate){
      const retorno =  toDate?.toISOString()?.split('T')[0].split("-");
      returnDate = `&va=${retorno[0]}&vm=${retorno[1]}&vd=${retorno[2]}`
    }
    if(!window) return pinbusURL
    return `${pinbusURL}?to=${from.id}&td=${to.id}&a=${salida[0]}&m=${salida[1]}&d=${salida[2]}${returnDate}`
}