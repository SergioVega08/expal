import React, { useEffect, useState } from "react"
import barra from "../../img/barra-amarilla.svg"
import { type } from "os"
import calendario from "../../img/calendario-solid.svg"
import geolocalizador from "../../img/geolocalizador.svg"
import { CSSProperties } from "react"
import { StationsType, City2Type, TerminalType, getRedirectURL } from "./pinbus"
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { Typeahead } from 'react-bootstrap-typeahead'; // ES2015
import { useMediaQuery } from 'react-responsive'
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { terminals } from "../../utils/terminals"
import DatePicker from 'react-datepicker';
import Image from 'next/image'

import styles from "./searchbanner.module.css";
import "./react_dates_overrides.module.css";

const Presets = {
    default: {
    },
    compact: {
        // width: '100%'
        borderBottomLeftRadius: '20px',
        borderBottomRightRadius: '20px'
    }
}



export type PresetTypes = keyof typeof Presets

export interface SearchBannerProps {
    style?: CSSProperties
    preset?: PresetTypes
    className?: string
}

const defaultStyle: CSSProperties = {
    width: '100%',
}

export const SearchBanner = (props: SearchBannerProps) => {
    const email = 'wlpalmira@pinbus.com'
    const password = 'wl_palmira_123'

    moment.locale('es-CO')
    const {
        style: styleOverride,
        className,
        preset = 'default'
    } = props

    const isMobile = useMediaQuery({
        query: '(min-device-width: 767px)'
    })

    const [show, setshow] = useState(preset == 'default' || isMobile)
    const [from, setFrom] = useState(null as City2Type)
    const [to, setTo] = useState(null as City2Type)
    const [toDate, setToDate] = useState(null)
    const [fromDate, setFromDate] = useState(null)
    const [data, setData] = useState(null as TerminalType)
    const [fromfocus, setfromfocus] = useState(false)
    const [tofocus, settofocus] = useState(false)




    const yesterday = new Date()
    yesterday.setDate(yesterday.getDate());
    const yesDate = yesterday.toISOString().split("T")[0]
    useEffect(() => {
        (async () => {
            const data = terminals
            setData({ content: data })
        })();

        // [...document.getElementsByClassName('search-date') as any].forEach(e => e.max = nowDate)

    }, [])
    // console.log(yesDate)

    // console.log(toDate?.toISOString().split('T')[0].split("-"), fromDate?.toISOString().split('T')[0])

    const style: CSSProperties = { ...Presets[preset], ...styleOverride }

    const _search = (e: any) => {
        const url = getRedirectURL({ from, to, toDate, fromDate, passengers: 1 })
        // console.log(url)
        window && window.open(url)
    }
    console.log(barra);
    return (
        // <Link to="/viajaseguro">
        <div className={`${styles['search-banner']} d-flex flex-column align-items-center ${className} ${preset}`} style={style} onClick={() => setshow(true)}>
            {preset == 'compact' ? <Image src={barra} alt="" /> : null}

            <div className={` ${styles['input-options']} d-flex flex-column flex-md-row align-items-center align-items-md-end m-b`}>
                <div className=" d-md-flex justify-content-around align-items-start w-100">
                    <div className={`d-flex flex-column ${styles['search-column']}`}>
                        <div className="d-flex flex-column">
                            <h1 className={`${styles.label} ${preset == 'compact' ? `${styles.compact} d-md-none` : ''}`}>¿A dónde viajas?</h1>
                        </div>
                        <div className={`d-md-flex`} >

                            <div className={styles['search-input']}>
                                <div className={`${styles.absoluteDiv} ${preset == 'compact' ? `${styles.iconPos}` : ''}`} >
                                    <Image src={geolocalizador} width={30} height={30} className={styles.PlaceholderIcon} alt="" />
                                </div>
                                <Typeahead
                                    id="from"
                                    // className="form-control"
                                    multiple={false}
                                    labelKey={e => e.name} //`${e.city.name}, ${e.city.code}`
                                    options={data && data.content || []}
                                    placeholder="Desde"
                                    onChange={e => setFrom(e[0])}
                                />
                            </div>

                            {show && <div className={`${styles['search-input']} form-group`}>
                                <Typeahead
                                    id="to"
                                    multiple={false}
                                    labelKey={e => e.name} //`${e.city.name}, ${e.city.code}`
                                    options={(data && data.content) || []}
                                    placeholder="Hasta"
                                    onChange={e => setTo(e[0])}
                                // selected={to}
                                />
                            </div>}
                        </div>
                    </div>
                    {show && <div className={`d-flex flex-column ${styles['search-column']}`}>
                        <div className="flex-column">
                            <h1 className={`${styles.label} ${preset == 'compact' ? `d-md-none ${styles.compact}` : ''}`}>¿Cuándo viajas?</h1>

                        </div>

                        <div className={`d-flex date-compact`} >
                            <div className="search-date d-flex w-50">
                                <Image src={calendario} width={30} height={30} className={styles.PlaceholderIcon} alt="" />
                                {/* <input type="date" className="form-control form-date from_date" placeholder="Fecha de ida" onChange={e => setFromDate(new Date(e.target.value))} /> */}
                                <DatePicker
                                    // className="form-control form-date from_date"
                                    selected={fromDate} // momentPropTypes.momentObj or null
                                    onChange={date => setFromDate(date)} // PropTypes.func.isRequired
                                    id="your_unique_id" // PropTypes.string.isRequired,
                                    required
                                />
                            </div>

                            <div className={`${styles['search-date']} d-flex w-50`}>
                                {/* <input type="date" className="form-control form-date to_date" placeholder="Regreso" onChange={e => setToDate(new Date(e.target.value))} /> */}
                                <DatePicker
                                    // className="form-control form-date from_date"
                                    selected={toDate} // momentPropTypes.momentObj or null
                                    onChange={date => setFromDate(date)} // PropTypes.func.isRequired
                                    id="your_unique_id" // PropTypes.string.isRequired,
                                    required={false}
                                />
                            </div>
                        </div>
                    </div>}
                </div>
                {show && <div className={`${styles['search-button-search']} my-md-3 my-4 my-md-0`}>
                    <button
                        // style={preset == 'default' ? {position: 'relative', bottom: '-2.5vw', zIndex: 10 } : {}}
                        onClick={_search}
                    >Buscar</button>
                </div>}
                {preset == 'default' ? <div className={styles.imgBarr + " " + styles.absoluteDiv}><Image src={barra} className={styles.BottomImage} alt="" /></div> : null}

            </div>
        </div>
        // </Link>
    )
}