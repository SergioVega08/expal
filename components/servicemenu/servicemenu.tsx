/* eslint-disable react/jsx-key */
import React from "react"
import styles from "./servicemenu.module.css"
import Link from 'next/link'
import documentos from "../../img/documentos-claro.svg"
import maletin from "../../img/maletin-claro.svg"
import mascota from "../../img/mascota-claro.svg"
import menores from "../../img/menores-claro.svg"
import portatil from "../../img/pc_wifi-claro.svg"
import ruta from "../../img/ruta-claro.svg"
import bus from "../../img/S26-recorte.png"
import Image from 'next/image'




export interface clientCard {
    image: any
    tittle: string
    link: string
}



export function ServiceMenu() {

    const cardservice: clientCard[] = [
        {
            image: documentos,
            tittle: "Contrato de transporte",
            link: "/servicio-cliente/contrato"
        },

        {
            image: maletin,
            tittle: "Manejo de equipaje",
            link: "/servicio-cliente/equipaje"
        },

        {
            image: mascota,
            tittle: "Transporte de mascotas",
            link: "/servicio-cliente/mascotas"
        },

        {
            image: menores,
            tittle: "Viaje con menores",
            link: "/servicio-cliente/menores"
        },

        {
            image: portatil,
            tittle: "Tiquetes en linea",
            link: "/servicio-cliente/onlinetiquets"
        },

        {
            image: ruta,
            tittle: "Política de tiquetes",
            link: "/servicio-cliente/politica"
        }
    ]

    function hover(element: any) {
        element.setAttribute('src', portatil)
    }
    const ItemService = ({ image, tittle, link, index }:
        { image: any, tittle: string, link: string, index: number }) => {
        return (
            <div className={`${styles['card-service']} d-flex flex-column col-5 col-md-3 align-items-center justify-content-around`}>
                <Link key={index} href={`${link}`}>
                    <Image src={image} alt="" />
                </Link>
                <Link key={index} href={`${link}`}>
                    <label>{tittle}</label>
                </Link>
            </div>
        )
    }
    // onMouseOver={hover(this)}
    return (
        <div className={`${styles['client-service']} d-flex align-items-center d-flex flex-wrap`}>
            <div className={`${styles['menu-client-service']} d-flex flex-wrap col-md-7`}>
                {cardservice.map((propieties, index: number) =>
                    <ItemService image={propieties.image} tittle={propieties.tittle} link={propieties.link} index={index}></ItemService>,
                )}

            </div>

            <div className={`${styles['client-service-bus']} col d-none d-md-block`}>
                <Image className="img-bus-service" src={bus} alt="" />
            </div>

            <div className={styles['client-service-fondo']}>

            </div>
        </div>
    )
}