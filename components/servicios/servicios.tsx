/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable @next/next/no-img-element */
import React from "react"
import {CSSProperties} from "react"
import baseViajero from "../../img/base_viajero.jpg"
import iconViajero from "../../img/icon-viajero.png"
import baseServicioAbordo from "../../img/base_servicioAbordo.jpg"
import iconServicioAbordo from "../../img/icon_servicioAbordo.png"
import iconHorarioViaje from "../../img/icon_horarioViaje.png"
import styles from "./servicios.module.css"

export function ServiciosContainer() {

    const styleServicio={
        backgroundColor: "rgb(57, 199, 47)"
    } as CSSProperties

    const styleHorario={
        color: "rgb(0, 128, 44)"
    } as CSSProperties

    const styleHorarioFondo={
        backgroundColor: "rgb(255, 233, 0)"
    } as CSSProperties

    return (
    
        <div className={styles.servicioContainer}>

            <a href="/ultra" className={styles.servicio}>
                <img className={styles.servicioBackground} src={baseViajero.src} alt="">
                </img>
                    <div className={styles.servicioInfo}>
                        <div className={styles.tiquete}>
                            <img className={styles.logo_tiquet} src={iconViajero.src} alt="">
                            </img>
                        </div>
                            <div className={styles.titulo_servicio}>
                                <p className={styles.cliente}>Cliente
                                </p>
                                <p className={styles.ultra}>Ultra
                                </p>
                            </div>
                        </div>
            </a>

            <a href="/buses/s26-maxxi-dupplo" className={styles.servicio}>
                    <img className={styles.servicioBackground} src={baseServicioAbordo.src} alt="">
                    </img>
                        <div className={styles.servicioInfo}>
                            <div className={styles.tiquete}>
                                <img className={styles.logo_tiquet} src={iconServicioAbordo.src} alt="" >
                                </img>
                            </div>
                                <div className={styles.titulo_servicio}>
                                    <p className={styles.cliente} style={styleServicio}>
                                            Servicios
                                    </p>
                                    <p className={styles.ultra} style={styleServicio}>
                                            A bordo
                                    </p>
                                </div>
                        </div>
            </a>
                            
            <a href="/rutas" className={styles.servicio}>
                <div className={styles.servicioBackgroundDiv} style={styleHorarioFondo}>
                </div>
                        <div className={styles.servicioInfo}>
                            <div className={styles.tiquete}>
                                <img className={styles.logo_tiquet} src={iconHorarioViaje.src} alt="">
                                </img>
                            </div>
                            <div className={styles.titulo_servicio}>
                                <p className={styles.cliente} style={styleHorario}>
                                    Horarios
                                </p>
                                <p className={styles.ultra} style={styleHorario}>
                                    de Viaje
                                </p>
                            </div>
                        </div>

            </a> 
            {/* <div className="servicioContainer">
            <div className="servicio-1 d-flex flex-column align-items-start justify-content-center col-md-3 pading">
                    <div className="tiquete">
                        <Image className="logo-tiquet" src={tiqueteAmarillo} alt="" />
                    </div>
                    <div className="titulo-servicio">
                        <p className="cliente">CLIENTE</p>
                        <p className="ultra">ULTRA</p>
                    </div>
                    <div className="restricciones">
                <label>*Aplica condiciones y restricciones.</label>
                    </div>
                </div>

                <div className="servicio-2 col-md-3 d-flex flex-column justify-content-center container-fluid">
                    <div className="logo-pasajero d-flex">
                        <img src={pasajeroBlanco}></img>
                    </div>
                <div className="descripcion">
                        <div className="servicios contenedor1 d-flex">
                            <label>SERVICIOS</label>
                        </div>
                        <div className="abordo contenedor2 d-flex">
                            <label>A  BORDO</label>
                        </div>
                </div>
                </div>

                <div className="servicio-3 col-md-3 col-md-auto d-flex flex-column align-items-start justify-content-center pading-3">
                    <div className="bus pading-img-bus">
                        <img src={busVerde}></img>
                    </div>
                    <div className="descripcion-servicio">
                        <label className="horarios">HORARIOS <br />
                            <label className="viaje">DE VIAJE</label> </label>
                    </div>
                </div>

            </div > */}
            </div>
    )

}