import React from "react"
import style from "./tiquetspolitic.module.css"



export function TiquetsPolitic({ data }: any) {
    return (
        <div className={style['content-politics']} dangerouslySetInnerHTML={{ __html: data.body }}></div>
    )
}