import React from "react";
import styles from "./viaja-tranquilo.module.css"
import { Model } from "../../types/basics";
import sa from "../../img/home_bus_footer.png";
import Image from 'next/image'
import Link from "next/link";

export function Viajatranquilo() {

    const data: Model = {
      textBanner: "Viaja tranquilo \n conociendo las \n políticas de viaje.",
      image: {
        src: sa.src,
      }
    }
    return (
      <div className={styles.banner_bus}>
        <div className={styles.img_bus}>
          <Image src={sa} alt="bus" />
        </div>
        <div className={styles.text_content}>
          <div className={styles.text}>
            {data.textBanner.split('\n').map((item, index) =>
              <label key={index}>{item}</label>
            )}
          </div>
          <div className="link">
            <Link href="/servicio-cliente/politica" passHref>
              <button className={styles.button_green}>Ver más</button>
            </Link>
          </div>
        </div>
      </div>
    );
  }