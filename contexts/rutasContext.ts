import { values, valuesIn } from "lodash";
import { createContext } from "react";
import { Bus } from "../types/basics";

export const RutasContext = createContext({
    routeSelected: null,
    setrouteSelected: (value: any) => { },
    buses: null as unknown as Bus[],
    busData: null as unknown as Bus
})
