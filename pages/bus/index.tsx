import React, { useState } from "react"
// import { Footer } from "../components/footer"
// import { SearchBanner } from "../../components/searchbanner"
import { ChoicedBus } from "../../components/choicedbus/choicedbus"
import { Labelindicator } from "../../components/labelindicator/labelindicator"
import Header  from '../../components/header/header'
import { useSiteData, useRouteData } from "react-static"
import { Bus,Route } from "../../types/basics"
 import { getImages } from "../..//utils/images"
import { RutasContext } from "../../contexts/rutasContext"
import styles from './bus.module.css'
import { Footer } from "../../components/footer/footer"
import { SearchBanner } from "../../components/searchbanner/searchbanner"

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
    // const { menuLinks } = useSiteData()
    // const { bus: busData, route: routeData } = useRouteData()
    // if (!busData || !routeData) return null

    // const bus: Bus = Object.values(busData.nodeBus)[0] as Bus
    // const routes: Route[] = Object.values(routeData.nodeRoute)
    // const image = getImages(busData, bus, 'image')[0]
    // const logo = getImages(busData, bus, 'logo') || [null]
    // const serviceImages = getImages(busData, bus, 'serviceImages')

    const [routeSelected, setrouteSelected,] = useState({ index: 0, original: { buses: ['']} })
    const busData = [{
        "name": "1"
    }]
    const title = 'Rutas y Horarios'
    const bus=[{
        bus: "S26",
        subTitle: "para viajer doblemente seguro",
        description: "Dotados con tecnología para su entretenimiento, con un ambiente interno amplio que proporciona confort y seguridad a los viajeros.",
        services: ["44 sillas reclinables",
        "Sistema Individual de Entretenimiento Digital Disfruta de contenidos, películas, música y aplicaciones",
        "Aire Acondicionado",
        "Luz Individual: ",
       " Para mejor comodidad, nuestro bus está equipado con luces individuales.",
        "Dos conductores calificados:Para tu tranquilidad las rutas de trayectos largos son operadas por 2 conductores.",
       " Conexión WI-FI: Accede de manera ilimitada a los servicios de Chat, Mail, y Redes sociales.",
        "G.P.S Seguimiento y Control Satelital",
        "Conectores: Conectores de energía para tus dispositivos.",
         "Baño"
        ],
        tabs: ["Primer Piso"]
    }]
     const image = {
         src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/principal-s26max.png"
     }
    
     const serviceImages =[
        {
           src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-10.jpg" 
        },
        {
            src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-09.jpg" 
        },
        {
            src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-01.jpg" 
        },
        {
            src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-05.jpg" 
        },
        {
            src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-02.jpg" 
        },
        {
            src:"https://s3.amazonaws.com/expresopalmira.cms.1/images/S26-Maxxi-06.jpg" 
        }
     ]
    const routes =[{}]
    return (
        <div>
            <Header/> 
            <SearchBanner style={{ position: 'relative', margin: 'auto', left: 0, right: 0, textAlign: 'center' }} preset='compact' />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Buses', url: '/buse' },
                ]}
                className={styles.bus_label_indicador}
            />
            {/* <RutasContext.Provider value={{ buses: [bus], busData, routeSelected, setrouteSelected }}>
                <ChoicedBus
                    bus={bus}
                    image={image}
                    logo={""}
                    serviceImages={serviceImages}
                    data={routes}
                    title={title}
                />
            </RutasContext.Provider> */}
            <Footer/>
        </div>
    )
}