import React from "react";
import { Banner } from "../../components/banner/banner";
import { Footer } from "../../components/footer/footer";
import Header from '../../components/header/header'
import {Historia} from '../../components/historia/historia'
import { Labelindicator } from '../../components/labelindicator/labelindicator'
import fundacionbg from "../../img/fundacion_foto.jpg"
import logoFundacion from "../../img/fundacion_logo.png"

export default function Fundacion(){
  const images = [
    {
        "web": "https://expal.com.co/static/fundacion_banner.8454aa89.jpg",
        "movil": "https://expal.com.co/static/fundacion_banner.8454aa89.jpg",
        "link": "",
    }]
    return(
        <>
        <Header/>
        <Banner images={images} tittle= "Fundación" subtittle="social" searchStatus={true} showTitle={true}/>
        <Labelindicator
              routes={[
                {label: 'Expreso Palmira', url: '/'},
                {label: 'Servicio al cliente', url: '/servicio-cliente'},
                {label: 'Fundación social', url: ''},
            ]}
            />
        <Historia
          mainDescription="<p>Expreso Palmira FUNSEP es una entidad sin ánimo de lucro creada por Transportes Expreso Palmira S.A con
          el fin de desarrollar programas sociales tendientes a mejorar la calidad de vida de los niños y jóvenes menos favorecidos a través de comedores comunitarios. Mediante el Transporte Social FUNSEP obtiene los recursos para beneficiar la niñez de los sectores menos favorecidos de Colombia, aportándoles el almuerzo cada día, buscando cumplir con el objetivo de dar almuerzo a 5000 niños de escasos recursos, FUNSEP ofrece los servicios de sus Unidades de Bienestar Social (UBS) son carrocerías adecuadas a las necesidades de los servicios requeridos como son unidad médica, comedor escolar, sala de cine entre otros.</p>"
          leftDescription="Las UBS se han convertido en el punto de partida del transporte social"
          rigthDescription="<p>Aprovechar los recursos de movilidad con los que cuenta Expreso Palmira S.A. estando en la capacidad de diseñar cualquier tipo de unidad que imprima dinámica al desarrollo de proyectos, eventos, campañas promocionales, etc.</p>"
          img={fundacionbg.src}
          topImage={logoFundacion.src}
        />
        <Footer/>
        </>

    )

}