/* eslint-disable import/no-anonymous-default-export */
import React from "react"
import { Footer } from "../../components/footer/footer";
import { Banner } from "../../components/banner/banner";
import { Mediosdepago } from '../../components/mediosdepago/mediosdepago';
import { Viajatranquilo } from "../../components/viaja-tranquilo/viaja-tranquilo"
import { ServiciosContainer } from "../../components/servicios/servicios"
import { SearchBanner } from "../../components/searchbanner/searchbanner"
// import { useRouteData, useSiteData } from "react-static"

import Header from '../../components/header/header';
// import { JSONApiBody, Image } from "../../../types/"
// import { Link } from "@reach/router"

export default () => {
    const images = [
        {
            "web": "https://s3.amazonaws.com/expresopalmira.cms.1/BannersHome/banner-web.png",
            "movil": "https://s3.amazonaws.com/expresopalmira.cms.1/BannersHome/banner-movil-web.png",
            "link": "",
        },
        {
            "web": "https://s3.amazonaws.com/expresopalmira.cms.1/images/bannerWeb_enero.png",
            "movil": "https://s3.amazonaws.com/expresopalmira.cms.1/images/bannerMovil_enero.png",
            "link": "",
        },
        {
            "web": "https://s3.amazonaws.com/expresopalmira.cms.1/images/bannerWeb_febrero.png",
            "movil": "https://s3.amazonaws.com/expresopalmira.cms.1/images/bannerMovil_febrero.png",
            "link": "",
        },
    ];

    return (
        <>
            <Header />
            {/* <Link to="viajaseguro"> */}
            <Banner images={images} showTitle={false} />
            {/* </Link> */}
            <SearchBanner style={{ margin: 'auto', position: 'relative', top: '-3vw' }} />

            <Mediosdepago />
            <ServiciosContainer />
            <Viajatranquilo />
            <Footer />
        </>
    )
}