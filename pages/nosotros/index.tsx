import React from "react";
import Header from '../../components/header/header'
import {Historia} from '../../components/historia/historia'
import { Labelindicator } from '../../components/labelindicator/labelindicator'
import { Experiencias } from '../../components/experiencias/experiencias'
import busbg from "../../img/nosotros-busbg.png"
import { Footer } from "../../components/footer/footer";
import { Banner } from "../../components/banner/banner";
import { SearchBanner } from "../../components/searchbanner/searchbanner";

export default function Nosotros(){
    const images = [
        {
            "web": "https://expal.com.co/static/preguntas_banner.be3812f6.png",
            "movil": "https://expal.com.co/static/preguntas_banner.be3812f6.png",
            "link": "",
        }]
    return(
        <>
        <Header/>
        <Banner images={images} tittle="Quienes" subtittle="somos" searchStatus={true} showTitle={true}/>
        <Labelindicator
                routes={[
                    {label: 'Expreso Palmira', url: '/'},
                    {label: 'Quienes somos', url: ''},
                ]}
            />
        <Historia
          title="Nuestra Historia"
          mainDescription="<p> Somos una compañía especializada en el transporte terrestre de pasajeros del sur occidente y centro de Colombia, creada el 17 de Marzo de 1956, contamos con más de 63 años de experiencia, tiempo en el que logramos reconocimientos que nos ubican como líderes en el servicio de transporte intermunicipal de pasajeros.</p>"
          leftDescription="Hemos desarrollado diferentes unidades de negocios, como son el transporte de carga, en comiendas y mensajería en modo terrestre, fluvial, marítimo y aéreo."
          rigthDescription="<p>
          Nos caracteriza la puntualidad, seguridad y comodidad en
          más de 400.000 despachos anuales, trabajando 24 horas y
          los 365 días del año, logrando que nuestros pasajeros lleguen a su destino a tiempo. <br /><br /> Contamos con vehículos modernos dotados con equipos de última tecnología permitiendo una mejor experiencia de viaje y calidad en el servicio, los conductores son altamente capacitados en la operación y seguridad de los vehículos, realizamos programas de mantenimientos preventivos que garantizan el óptimo estado de nuestra flota.</p>"
          img={busbg.src}
        />
        <Experiencias/>
        <Footer/>
        </>

      
    )


}