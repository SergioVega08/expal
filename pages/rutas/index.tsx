/* eslint-disable import/no-anonymous-default-export */
import React, { useState } from "react"
import { Footer } from "../../components/footer/footer"
import { Banner } from "../../components/banner/banner"
import { Labelindicator } from "../../components/labelindicator/labelindicator"
import { Mapsidebar } from "../../components/mapsidebar"
import { Rutas } from "../../components/rutas"
import banner from "../../img/rutas_banner.png"
import mapa from "../../img/rutas_mapa-01.svg"
import styles from "./styles.module.css"
import Header from "../../components/header/header"
import { RutasContext } from "../../context/routesCntext"
// import { BusCarousel } from "components/busCarousel"


export default () => {
    // const { route: routeData, bus: busData } = useRouteData()
    // if (!routeData || !busData) return null

    const title = 'Rutas Disponibles'
    // const routes: JSONApiBody<Route>[] = Object.values(routeData.nodeRoute)
    // const buses: JSONApiBody<Bus>[] = Object.values(busData.nodeBus)
    const buses = [{
        "name": "1"
    }]
    const busData = [{
        "name": "1"
    }]
    const routes = [{}];

    const [routeSelected, setrouteSelected,] = useState({ index: -1, original: { buses: [''] } })
    const bus = buses[0]
    // console.log(bus)

    return (
        <>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Rutas"
                subtittle="y destinos"
                searchStatus={true}
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Rutas y destinos', url: '' },
                ]}

            />

            <Mapsidebar />
            <div className={`${styles['box-info']} d-flex flex-md-row flex-column `}>
                <RutasContext.Provider value={{ routeSelected, setrouteSelected, buses, busData }}>
                    <Rutas style={{ minWidth: '50vw' }} routes={routes} title={'Rutas Disponibles'} columns={['src', 'dst']} />
                    {routeSelected.index != -1 ?
                        <div className={`d-none d-md-block w-100 styles['busRutasContainer']`}>
                            {/* <BusCarousel routeSelected={routeSelected} /> */}
                        </div>
                        : null}
                </RutasContext.Provider>
            </div>

            <Footer />

        </>
    )
}