/* eslint-disable react/display-name */
/* eslint-disable import/no-anonymous-default-export */
import React, { useEffect } from "react"
import { useSiteData, useRouteData } from "react-static"
import Header  from "../../components/header/header"
import { Footer } from "../../components/footer/footer"
import { Banner } from "../../components/banner/banner"
import { Labelindicator } from "../../components/labelindicator/labelindicator"
import { ContainerInfoSedes } from "../../components/containerinfosedes/containerinfosedes"


// import {scrollTop} from "../../utils"

import banner from "../../img/sedes_banner.jpg"
import sucursalArmenia from "../../img/sedes_terminal_armenia.jpg"
// import { Sede, JSONApi, JSONApiBody } from "../../../types/"

export const scrollTop = () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
}

export default () => {
    // const { menuLinks } = useSiteData()

    useEffect(() => {
        scrollTop()
    }, [])
    
    const images = [
        {
            "web": "https://expal.com.co/static/sedes_banner.1eb0ebd0.jpg",
            "movil": "https://expal.com.co/static/sedes_banner.1eb0ebd0.jpg",
            "link": "",
        }]
    return (
        <div>
            <Header/>
            <Banner
                images={images}
                tittle="18 /n Agencias"
                subtittle="en el país"
                subtittlecolor="#5d5d5d"
                searchStatus={true}
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Agencias en el país', url: '' },
                ]}
            />
            {/* <ContainerInfoSedes /> */}
            <Footer/>
        </div>
    )

}