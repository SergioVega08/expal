import React, { useEffect } from "react"
import Header from "../../../components/header/header"
// import { InisioSesion } from "../../../components/"
import { Footer } from "../../../components/footer/footer"
import { scrollTop } from "../../../utils/pages"

export default () => {

    useEffect(() => {
        scrollTop()
    }, [])

    return (
        <>
            <Header />
            {/* <InisioSesion /> */}
            <Footer />
        </>
    )
}