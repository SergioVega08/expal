import React, { useEffect } from "react"
// import { useRouteData, useSiteData } from "react-static"
import Header from "../../../components/header/header"
import { Footer } from "../../../components/footer/footer"
import { Banner } from "../../../components/banner/banner"
import { Labelindicator } from "../../../components/labelindicator/labelindicator"
import { InsideServiceClient } from "../../../components/insideserviceclient/insideserviceclient"
import { ContractContent } from "../../../components/contractcontent"
import banner from "../../../img/contrato-banner.png"
import { scrollTop } from "../../../utils/pages"

export default () => {
    // const { menuLinks, pages } = useSiteData()
    useEffect(() => {
        scrollTop()
    }, [])
    const data = {
        'body': '<p>Con este tiquete se celebra un contrato de transporte el cual se basa bajo los regímenes del Código de comercio (CCo.) y del Ministerio de Transporte.<br> El artículo 1000 de CCo que estipula la obligación de cumplir los reglamentos de la empresa.</p> <hr />',
        'dropdown': '<h3>Detalles de viaje</h3><ul><li>Este tiquete es válido para el cliente, de acuerdo a las especificaciones impresas en él. Cualquier indicación que aparezca en el tiquete de viaje hecha por el pasajero o por los empleados o dependientes de la empresa o por terceros que modifique, cambie de algún modo, las cláusulas que aquí aparecen o las especificaciones impresas carecerán de validez.</li>'
        +'<li>El pasajero debe presentarse en el lugar de iniciación del viaje, media hora antes de la indicada en el tiquete.</li>'
        +'<li>La empresa no responde por demoras, daños, pérdidas, etc. que se ocasionen por fuerza mayor, caso fortuito, conducta de un tercero o por culpa del pasajero.</li>'
        +'<li>La empresa no transportará personas que se encuentren en estado de embriaguez o bajo el influjo de estupefacientes o en notorio estado de desaseo.</li>'
        +'<li>El pasajero está obligado a observar las condiciones de seguridad que la empresa determine y a tomar todas las medidas necesarias a fin de evitar daños o accidentes.</li>'
        +'<li>El pasajero acepta el seguro que la empresa tiene contratado con una compañía de seguros por los riesgos de muerte, lesiones personales y gastos médicos.</li>'
        +'<li>El pasajero puede solicitar cambio de las condiciones hasta dos horas antes de la hora de inicio del recorrido; los cambios sólo podrán realizarse por servicios autorizados y prestados por la empresa y se podrán solicitar únicamente en las taquillas de la empresa, el pasajero debe pagar el mayor valor sí existiere.</li>'
        +'<li>El derecho de retracto puede ejercerse por el pasajero hasta dos horas antes de la hora fijada para iniciar el recorrido. Si el aviso de retracto por parte del pasajero no sucede antes de las dos horas previas a iniciar el recorrido, no se hará devolución del valor pagado por el servicio. Parágrafo: Cuando la empresa ofrezca promociones por trayecto (Ida-Regreso) el Derecho de Retracto se entenderá ejercido al inicio del trayecto.</li>'
        +'<li>La empresa no responde por demoras de itinerarios que se ocasionen por fuerza mayor o caso fortuito, de igual forma el nivel de servicio asignado puede cambiar de acuerdo con la disponibilidad del parque automotor.</li>'
        +'<li>SERVICIOS ADICIONALES: Para aquellos vehículos que cuentan con tecnología abordo, el cliente podrá navegar por Internet a través del router y WIFI instalado en el vehículo, utilizando los puntos de acceso-APN “Access Point Network” proveedor de servicios de comunicaciones, a las velocidades contratadas, de acuerdo al área de cobertura de la zona por donde el vehículo se encuentra transitando, cuyas velocidades y calidad del servicio no depende de la empresa de transporte, quien ofrece este servicio de navegación como un valor agregado al servicio el cual no se cobra.</li></ul>'
        +'<h3>Menores de Edad</h3><p>La empresa no responde por la custodia o cuidado de menores de edad o incapaces, pues estos deberán estar a cargo de sus representantes. El pasajero declara ser mayor de edad.</p>'
        +'<h3>Equipaje</h3><p>Cada pasajero podrá llevar sin costo una maleta que contengan bienes de uso personal (que no incluye elementos de valor como joyas, dinero, mercancía, entre otras) en bodega con medidas que no superen los 80 x 50 x 30 cms y un peso máximo de 25 kg y en cabina un maletín de dimensiones 60 x 40 x 20 cms y un peso de 10 kg máximo. Si el equipaje a transportar excede las medidas o el peso establecido por la empresa, el pasajero cancelará un valor adicional por el uso de la bodega.</p>'
        +'<h3>Autorización tratamiento de datos personales</h3><p>De conformidad con lo previsto en la Ley 1581 de 2012, Decreto 1377 de 2013 y todas las demás que los adicionan y complementan autorizo para tratar mis datos personales necesarios y requeridos para formalizar el contrato de transporte, tales como nombre e identificación, esta información será para uso único y exclusivo del Transportador, la cual hará parte de su base de datos en virtud de la compra del tiquete de viaje , de igual forma estará facultada para recolectar, ceder, transferir, archivar y mantener mis datos personales y la copia de mi cédula para las finalidades que requiere la celebración del presente contrato y para todas aquellas inherentes al mismo conforme a la Política de Tratamiento de Datos Personales en la empresa disponible www.expresopalmira.com.co donde también podrá consultar sus derechos, reclamos, consultas supresión y demás aspectos inherentes al tratamiento de sus datos personales. Mi autorización se entenderá efectuada y aceptada con la compra del presente tiquete de viaje.</p>'
        +'<h3>Transporte de mascotas</h3><p>Sólo se permitirá el transporte de mascotas (Perros o Gatos), cuando sean perros lazarillos, y lleven su respectiva tradilla y bozal, también mascotas pequeñas o medianas que no sobrepasen los 20 Kg, de peso, los cuales solo se transportarán en guacales suministrados<br>'
        +'por el pasajero. Las mascotas grandes o de más de 20 Kg de peso, sólo se transportaran en rutas de trayecto largo y se transportaran en la bodega del bus, con bozal, tradilla, en guacal suministrados por el pasajero. Deberán portar el carnet de vacunas. La responsabilidad del<br>'
        +'animal será del pasajero.</p><h3>Aceptación</h3><p>El usuario al recibir y hacer uso del pasaje o por el simple hecho de ser transportado por o a nombre de Transportes Expreso Palmira, se declara conforme con los términos del presente Contrato. La no aplicación o invalidez de alguna de las cláusulas no afecta la validez del resto<br>'
        +'del Contrato. Para dudas e inquietudes comuníquese al correo electrónico <a href="mailto:servicioalcliente@expresopalmira.com.co">servicioalcliente@expresopalmira.com.co</a> o a la Línea Gratuita 01 8000 93 66 62 .</p> '
    }
    return (
        <div>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Contrato"
                subtittle="de transporte"
                subtittlecolor="#00802c"
                searchStatus
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '/servicio-cliente' },
                    { label: 'Contrato de transporte', url: '' },
                ]}
            />
            <InsideServiceClient
                contentLayout={() => ContractContent({ data })}
            />
            <Footer />

        </div>
    )

}