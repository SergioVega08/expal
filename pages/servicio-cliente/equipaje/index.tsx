import React, { useEffect } from "react"
import Header from "../../../components/header/header"
import { Footer } from "../../../components/footer/footer"
import { Banner } from "../../../components/banner/banner"
import { Labelindicator } from "../../../components/labelindicator/labelindicator"
import { InsideServiceClient } from "../../../components/insideserviceclient"
import { MaletasContent } from "../../../components/maletascontent"
import banner from "../../../img/equipaje-banner.png"
import { scrollTop } from "../../../utils/pages"

export default () => {

    useEffect(() => {
        scrollTop()
    }, [])
    const data={
        'body':'<h3>Manejo de equipaje</h3><p>Cada pasajero tiene derecho a dos (2) maletas:</p><hr /><h4>Nota: Para los servicios METTRO y METTRO X, la maleta grande no puede superar los 60cm x 40cm x 20cm.</h4>'
        +'<h4>Equipaje Adicional:</h4><p>Cada pasajero puede llevar 1 maleta adicional que no supere las medidas 60cm x 40cm x 20cm comprando un bono de $10.000 pesos en temporada baja y $15.000 pesos en temporada alta, p’ara los servicios S26, MAXXI y DUPPLO; para los servicios METTRO y METTRO X no se permite equipaje adicional por la capacidad de la bodega del vehículo.</p>'
        +'<h4>* Temporada Alta:</h4><p>Semana Santa (comenzando el viernes anterior al domingo de ramos hasta el lunes de pascua), del 15 de Junio al 31 de Julio, semana de receso escolar en Octubre y del 15 de Noviembre al 31 de Enero del año siguiente.</p>'
    }
    return (
        <div>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Manejo"
                subtittle="de equipaje"
                subtittlecolor="#00802c"
                searchStatus
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '/servicio-cliente' },
                    { label: 'Manejo de equipaje', url: '' },
                ]}
            />

            <InsideServiceClient
                contentLayout={() => MaletasContent({ data })}
            />

            <Footer />

        </div>
    )

}