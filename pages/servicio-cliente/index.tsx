import React from "react"
import Header from "../../components/header/header"
import { Footer } from "../../components/footer/footer"
import { Banner } from "../../components/banner/banner"
import { Labelindicator } from "../../components/labelindicator/labelindicator"
import { InputSearch } from "../../components/inputSearch"
import { ServiceMenu } from "../../components/servicemenu"
import banner from "../../img/servicio-al-cliente-banner.png"

export default () => {
    return (
        <div>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Servicio"
                subtittle="al cliente"
                subtittlecolor="#00802c"
                searchStatus={true}
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '' },
                ]}
            />
            <InputSearch />
            <ServiceMenu />
            <Footer />


        </div>
    )

}
