import React, { useEffect } from "react"
import Header from "../../../components/header/header"
import { Footer } from "../../../components/footer/footer"
import { Banner } from "../../../components/banner/banner"
import { Labelindicator } from "../../../components/labelindicator/labelindicator"
import { InsideServiceClient } from "../../../components/insideserviceclient"
import { MascotasContent } from "../../../components/mascotascontent"
import banner from "../../../img/mascota_banner.png"
import { scrollTop } from "../../../utils/pages"
export default () => {


    useEffect(() => {
        scrollTop()
    }, [])
    const data = {
        'body': '<h3>Transporte de Mascotas</h3><p>Este servicio se ofrece dependiendo del tipo de bus, la disponibilidad y el tamaño del las mascota, la empresa podrá&nbsp;negarse al transporte de la mascota.</p><p>El transporte de mascotas se realiza con autorización del pasajero y bajo su responsabilidad.</p><p>La empresa no asume ninguna responsabilidad por los daños que se ocasionen durante el viaje.</p><p>Las mascotas solo están autorizadas para viajar en la bodega del vehículo.</p><h4>Requisitos:</h4><ul>    <li>Guacal (proporcionado por el pasajero).</li>    <li>Portar el carnet de vacunas.</li>    <li>Sedado.</li>    <li>Comprar el bono de mascotas en la taquilla.</li></ul><p>Las mascotas pequeñas (hasta 20cms de altura) se permiten viajar en el interior del vehículo en rutas cortas*.</p><h4>Requisitos:</h4><ul>    <li>Bolso porta mascota.</li>    <li>Pañal.</li>    <li>Carnet de vacunas.</li></ul><p>Prohibido que la mascota salga del porta mascotas durante el viaje en caso que ocurra, el pasajero deberá abandonar el vehículo.</p><h4>* Rutas Cortas:</h4>'
    }
    return (
        <div>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Transporte"
                subtittle="de mascotas"
                subtittlecolor="#00802c"
                searchStatus
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '/servicio-cliente' },
                    { label: 'Transporte de mascotas', url: '' },
                ]}
            />
            <InsideServiceClient
                contentLayout={() => MascotasContent({ data })}
            />
            <Footer />

        </div>
    )

}