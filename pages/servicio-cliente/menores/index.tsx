import React, { useEffect } from "react"
import Header from "../../../components/header/header"
import { Footer } from "../../../components/footer/footer"
import { Banner } from "../../../components/banner/banner"
import { Labelindicator } from "../../../components/labelindicator/labelindicator"
import { InsideServiceClient } from "../../../components/insideserviceclient"
import { MenoresContent } from "../../../components/menorescontent"
import { scrollTop } from "../../../utils/pages"

import banner from "../../../img/menores_banner.png"

export default () => {

    useEffect(() => {
        scrollTop()
    }, [])
    const data = {
        'body': '<h3>Transporte de menores</h3><p>Los menores de edad con una altura igual o superior a 90cm deben ocupar silla individual y comprar su tiquete de su viaje</p><div><label>Los menores a 15 años</label><h4>deben viajar con un adulto</h4></div><p>Los menores de edad igual o mayor a 15 años pueden viajar sin acompañante presentando una carta de autorizacion y fotocopia de la cedula del adulto que autoriza.</p>'
    }
    return (
        <div>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Viaje"
                subtittle="con menores"
                subtittlecolor="#00802c"
                searchStatus
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '/servicio-cliente' },
                    { label: 'Viaje con menores', url: '' },
                ]}
            />
            <InsideServiceClient
                contentLayout={() => MenoresContent({ data })}
            />
            <Footer />
        </div>
    )
}