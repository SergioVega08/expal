import React, { useEffect } from "react"
import Header from "../../../components/header/header"
import { Footer } from "../../../components/footer/footer"
import { Banner } from "../../../components/banner/banner"
import { Labelindicator } from "../../../components/labelindicator/labelindicator"
import { InsideServiceClient } from "../../../components/insideserviceclient"
import { OnlineContent } from "../../../components/onlinecontent"
// import { scrollTop } from "../../../utils/pages"
import banner from "../../../img/tiquetes_banner.png"

export default () => {

    // useEffect(() => {
    //     scrollTop()
    // }, [])
    const data = {
        'body': '<h3>Momento de la emisión</h3><ul><li>En el momento de emisión del comprobante de compra de pasaje vía electrónica,'
            + 'Ud. deberá verificar: Lugar de salida, fecha y hora de viaje, precio abonado, categoría del servicio y destino, conforme lo'
            + 'solicitado, sin derecho a posteriores reclamos, debe validar que su correo electrónico este activo y bien escrito ya que allí llegara'
            + 'la información de su compra.</li><li>El pasajero debe presentarse en el lugar de iniciación del viaje, media hora antes de la indicada'
            + 'en el tiquete.</li><li>Debe presentarse en el despacho de transportes Expreso Palmira de la ciudad origen con aproximadamente 45 minutos'
            + 'de anterioridad a la hora estipulada de salida del vehículo.</li></ul><h3>Canjear el comprobante de compra</h3>'
            + '<p>Para canjear el comprobante de compra de pasaje impreso por el tiquete de viaje definitivo al reverso de este encontrará el contrato de '
            + 'transporte. De no presentarse en el tiempo indicado su reserva se dará de baja automáticamente y su puesto quedara disponible para la venta. '
            + 'Para poder ser canjeado el comprobante de compra de pasaje por el tiquete de viaje, presentar cédula, Tarjeta de identidad, registro civil o pasaporte '
            + 'Original del viajero.</p> <h3>Compra NO aprobada</h3><p>En caso de que la operación y/o compra NO sea APROBADA, sea Rechazada, Denegada o surja algún error '
            + 'al validar los fondos o datos de la tarjeta, en este caso se pierde la reserva de las sillas debiendo el pasajero volver a iniciar la operación desde el principio.</p>'
            + '<h3>Política de tratamiento de datos</h3><p>Al momento del que el viajero diligencie el formulario de datos personales, está aceptando la política de '
            + 'tratamiento de datos de Transportes Expreso Palmira.</p><h3>Derecho de retracto</h3><p>Para los efectos de lo previsto en el artículo 47 de la ley 1480 '
            + 'de 2011. El usuario podrá retractarse de la compra efectuada hasta un término máximo de 60 minutos antes de la hora y fecha programada para el viaje. '
            + 'Para tal efecto deberá enviar su solicitud al correo electrónico <a href="mailto:servicioalcliente@expresopalmira.com.co">servicioalcliente@expresopalmira.com.co</a> '
            + 'La devolución del dinero pagado por la compra se hará por el mismo medio que se utilizó para el pago. Por la naturaleza y condiciones del servicio ofrecido no se podrá '
            + 'hacer uso del derecho de retracto por fuera del término establecido en las presentes condiciones.</p>'
            + '<h3>Condiciones de transporte</h3><p>La empresa no responde por demoras de itinerarios que se ocasionen por fuerza mayor o caso fortuito, de igual '
            + 'forma el nivel de servicio asignado puede cambiar de acuerdo con la disponibilidad del parque automotor.<br>'
            + 'El pasajero debe presentarse en el lugar de iniciación del viaje, media hora antes de la indicada en el tiquete.<br>'
            + 'El usuario al recibir y hacer uso del pasaje o por el simple hecho de ser transportado por o a nombre de Transportes Expreso Palmira, se declara '
            + 'conforme con los términos del presente Contrato. La no aplicación o invalidez de alguna de las cláusulas, no afecta la validez del resto del Contrato. '
            + 'Cualquier indicación que aparezca en el tiquete, hecha por el usuario o por empleados o dependientes de la empresa o por terceros y tienda a cambiar, '
            + 'modificar o suspender de algún modo las cláusulas que aquí aparecen, carecerá de validez.</p>'
    }
    return (
        <div>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Tiquetes"
                subtittle="en linea"
                subtittlecolor="#00802c"
                searchStatus={true}
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '/servicio-cliente' },
                    { label: 'Tiquetes en línea', url: '' },
                ]}
            />
            <InsideServiceClient
                contentLayout={() => OnlineContent({ data })}
            />
            <Footer />

        </div>
    )

}