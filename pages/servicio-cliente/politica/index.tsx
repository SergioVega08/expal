import React, { useEffect } from "react"
import Header from "../../../components/header/header"
import { Footer } from "../../../components/footer/footer"
import { Banner } from "../../../components/banner/banner"
import { Labelindicator } from "../../../components/labelindicator/labelindicator"
import { InsideServiceClient } from "../../../components/insideserviceclient"
import { TiquetsPolitic } from "../../../components/tiquetspolitic"
import banner from "../../../img/politica-banner.png"
import { scrollTop } from "../../../utils/pages"

export default () => {

    useEffect(() => {
        scrollTop()
    }, [])

    const data = {
        'body': '<h3><span><span><span><span><span><span>TÉRMINOS &amp; CONDICIONES DE VIAJE</span></span></span></span></span></span></h3>'
        +'<ol>'
        +'<li><span><span><span><span><span><span><span><span>El usuario acepta que su información será tratada conforme a la Política de tratamiento de datos adoptada por Expreso Palmira S.A y que podrá consultar en la página web www.expresopalmira.com.co y que conforme a la misma tendrá derecho a solicitar la supresión, corrección o modificación de los mismos, mediante correo electrónico enviado al buzón </span></span></span><span><a href="mailto:servicioalcliente@expresopalmira.com.co"><span><span>servicioalcliente@expresopalmira.com.co</span></span></a></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>De igual forma con la inscripción realizada se acepta que para efectos de los servicios solicitados podrá ser contactado por cualquier medio válido tal como correo electrónico, llamada telefónica, SMS, WhatsApp Business o en general cualquier medio válido y con el cual se pueda lograr comunicación efectiva, por tanto, es responsabilidad del usuario brindar información veraz para efectos de ser contactado.</span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>Expreso Palmira tiene una disponibilidad de ocupación de sillas del 70% del vehículo, con el fin de cumplir con los respectivos protocolos de bioseguridad.&nbsp;</span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span>La empresa no responde por la custodia o cuidado de menores de edad o incapaces, pues estos deberán estar a cargo de sus representantes. El pasajero declara ser mayor de edad.</span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>El Derecho al retracto operará para compras no presenciales, según los términos previstos en el Artículo 47 Ley 1480 del 2011 dentro de los cinco (05) días siguientes a la fecha de la compra. En todo caso la empresa acepta el desistimiento del viaje por parte del pasajero, con derecho a la devolución del dinero pagado, siempre y cuando éste se realice por lo menos con dos horas de antelación a la programación de su viaje.&nbsp; </span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>Las personas que tomen el servicio deberán contar con tapabocas, aceptar y someterse a todos los protocolos de bioseguridad&nbsp;realizados en el punto de partida.&nbsp;</span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span><span>En caso de no poder viajar, debe notificarlo al correo electrónico servicioalcliente@expresopalmira.com.co con al menos 2 horas de antelación a la fijada para la salida y así poder reprogramar su viaje en un periodo máximo de 90 días. El pasajero que no efectúe el viaje y no realice la notificación anterior, pierde el derecho a la devolución del dinero. No obstante, si la falta de presentación tiene lugar por causa de fuerza mayor, el pasajero tendrá derecho a que se le devuelvan las cantidades entregadas. Para estos efectos, se considerará causa de fuerza mayor la muerte, el accidente o enfermedad graves del pasajero o de alguna de las personas con quien conviva o cualquier supuesto análogo que le impida realizar el viaje y comunicar a la agencia esa imposibilidad en el término fijado. </span></span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span><span>En caso de cancelación del viaje por parte de Expreso Palmira S.A, por cualquier motivo que no sea imputable al pasajero, éste tendrá derecho; según sea su elección para desistir del contrato y solicitar el reembolso del 100% de lo cancelado al momento de la reserva o la compra o a la reprogramación de su viaje en un plazo de hasta 90 días. </span></span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>Es necesario llegar 60 minutos antes de la hora programada de salida, para poder someterse al protocolo de bioseguridad.</span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>En caso de tener un tiquete pendiente de Expreso Palmira S.A el cual no haya sido posible utilizar, se podrá usar para ser canjeado como parte de pago del valor del nuevo tiquete (En caso de ser un valor mayor el cliente deberá asumir la diferencia, en caso de ser menor de acuerdo con lo expuesto en el contrato de transporte no habrá lugar a reintegro del valor diferencial).</span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>Expreso Palmira no es responsable por la decisión de autoridades competentes, que en el transcurso del viaje puedan suspenderlo o retrasarlo.</span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span>Cada pasajero podrá llevar sin costo una maleta que contengan bienes de uso personal (que no incluye elementos de valor como joyas, dinero, mercancía, entre otras) en bodega con medidas que no superen los 80 x 50 x 30 cms y un peso máximo de 25 kg y en cabina un maletín de dimensiones 60 x 40 x 20 cms y un peso de 10 kg máximo. Si el equipaje a transportar excede las medidas o el peso establecido por la empresa, el pasajero cancelará un valor adicional por el uso de la bodega.</span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span>El valor del equipaje aceptado por la empresa es de cien mil pesos ($100.000) por cada maleta. En caso de pérdida, este valor se pagará únicamente cuando el equipaje ha sido entregado al conductor y se transporte en las bodegas del bus con su respectiva “ficha de equipaje” cuyo número debe ser igual a la ficha adherida en la valija.</span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>En caso de presentar preguntas, quejas o reclamos puede ponerlas por el formulario de nuestra página web o vía correo electrónico: servicioalcliente@expresopalmira.com.co</span></span></span></span></span></span></span></span></li>'
        +'<li><span><span><span><span><span><span><span><span>Para envío de notificaciones judiciales debe ser por medio del&nbsp;correo electrónico:&nbsp;contabilidad@expresopalmira.com.co&nbsp;</span></span></span></span></span></span></span></span></li>'
        +'</ol>'
    }
    return (
        <div>
            <Header />
            <Banner
                images={[banner.src]}
                tittle="Política"
                subtittle="de tiquetes"
                subtittlecolor="#00802c"
                searchStatus
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '/servicio-cliente' },
                    { label: 'Política de tiquetes', url: '/servicio-cliente/politica' },
                ]}
            />

            <InsideServiceClient
                contentLayout={() => TiquetsPolitic({ data })}
            />

            <Footer />

        </div>
    )

}