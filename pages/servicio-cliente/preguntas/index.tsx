import React, { useEffect } from "react"
import { useRouteData, useSiteData } from "react-static"
import { Header } from "../../../components/header"
import { Footer } from "../../../components/footer"
import { Banner } from "../../../components/banner"
import { Labelindicator } from "../../../components/labelindicator"
import { InsideServiceClient } from "../../../components/insideserviceclient"
import { PreguntasContent } from "../../../components/preguntascontent"

import banner from "../../../img/preguntas-banner.jpg"
import { scrollTop } from "../../../utils"

export default () => {
    const { menuLinks } = useSiteData()
    const { faq } = useRouteData()
    // console.log(faq)
    if (!faq) return null

    useEffect(() => {
        scrollTop()
    }, [])

    return (
        <div>
            <Header data={menuLinks} />
            <Banner
                images={[banner]}
                tittle="Preguntas"
                subtittle="frecuentes"
                subtittlecolor="#00802c"
                searchStatus
            />
            <Labelindicator
                routes={[
                    { label: 'Expreso Palmira', url: '/' },
                    { label: 'Servicio al cliente', url: '/servicio-cliente' },
                    { label: 'Preguntas frecuentes', url: '' },
                ]}
            />

            <InsideServiceClient
                contentLayout={PreguntasContent}
            />

            <Footer data={menuLinks} />

        </div>
    )

}