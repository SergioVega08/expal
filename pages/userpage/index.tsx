import React from 'react'
import Header from '../../components/header/header'
import { AcumulatedPoints } from '../../components/acumulatedpoints/acumulatedpoints'

export default function UserPage() {

    const userData ={
        username: "Ganar es muy facil",
        travelKm: "200",
        from: "Cali",
        to: "Pereira"
      }
    return(
        <>
          <Header/>
          <AcumulatedPoints data={userData}/>
        </>
    )
    
}
