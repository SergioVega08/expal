// import { Node, TextFormatted, Link, Color } from '.'
import { ImageProps } from "next/image";
// import { Node, TextFormatted, Link, Color } from ""


export interface HistoriaProps {
    title?: string
    mainDescription: string
    leftDescription: string
    rigthDescription: string
    img: string
    topImage?: string
}
export interface LabelProps {
    routes: {
        label: string,
        url: string
    }[]
    // style?: CSSProperties
    className?: string
}

export interface Model {
    textBanner: string
    image: {
      src: string
      //style: CSSProperties
    }
  }
export interface TextFormatted {
    value: string
    format: string
    processed: string
}
export interface InfoSedeProps {
    title: string
    sede: { city: string
            location: string
            address: string
            officeHours: string
            phone: string
        }
    images?:{
            value: string
            url: string
        }
 }
 
export interface Sede extends Node {
    city: string
    location: string
    address: string
    officeHours: string
    phone: string
}
 
export interface ListSedesProps {
    sucursales: string[]
    onCity: (index: string) => void
}

export interface Route {
    src: string
    dst: string
}

export interface UserModel {
    data: {
        username: string
        travelKm: string
        from: string
        to: string
    }
}
export interface Image{
    uri: {
        value: string
        url: string
    }   
}

export interface Bus {
    bus: string
    subTitle: string
    description: string
    services: string[]
    tabs: string[]
}