/* eslint-disable react-hooks/rules-of-hooks */
import { useSiteData } from "react-static"
import {  Image} from '../types/basics'

export function getImageURL(uri: string): string {
    const { contentURL } = useSiteData()
    return contentURL + uri
}

export function getImages(data: any, node: any, image: string): Image[] {
    const images = data.files
    const dat = node.relationships[image].data
    if (!dat) return []
    if (Array.isArray(dat)) return dat.map(d => images[d.id])
    else return [images[dat.id]]
}