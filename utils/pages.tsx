import { ReactNode } from "react";


export const scrollTop = () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
}