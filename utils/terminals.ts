export const terminals = [
  {
    id: 1,
    name: "Ter. PEREIRA"
  },
  {
    id: 2,
    name: "Ter. BOGOTA SALITRE"
  },
  /* {
    id: 3,
    name: "Ter. BUCARAMANGA"
  }, */
  {
    id: 4,
    name: "Ter. MANIZALES"
  },
  {
    id: 5,
    name: "Ter. ARMENIA"
  },
  {
    id: 6,
    name: "Ter. TULUA"
  },
  /* {
    id: 7,
    name: "Ter. BUGA-"
  }, */
  /* {
    id: 8,
    name: "Ter. BUENAVENTURA"
  }
  , */
  {
    id: 9,
    name: "Ter. CAICEDONIA"
  }
  ,
  /* {
    id: 10,
    name: "Ter. BARRAGAN"
  }
  , */
  {
    id: 11,
    name: "Ter. SEVILLA"
  }
  ,
  /* {
    id: 12,
    name: "Var. TULUA"
  } */
  ,
  {
    id: 13,
    name: "Ter. BUGA"
  }
  ,
  {
    id: 14,
    name: "PALMIRA Versalles"
  }
  ,
  {
    id: 15,
    name: "Ter. CALI"
  }
  ,
  {
    id: 16,
    name: "Ter. MEDELLIN"
  }
  ,
  {
    id: 17,
    name: "Ter. POPAYAN"
  }
  ,
  /* {
    id: 18,
    name: "Ter. SANTANDER DE QUILICHA"
  }
  , */
  /* {
    id: 19,
    name: "Ter. PIENDAMO"
  }
  , */
  {
    id: 20,
    name: "Ter. CARTAGO"
  }
  ,
  {
    id: 21,
    name: "Ter. IBAGUE"
  }
  ,
  {
    id: 22,
    name: "Ter. BOGOTA SUR"
  }
  ,
  /* {
    id: 23,
    name: "PALMIRA Estacion"
  }
  ,
  {
    id: 24,
    name: "Var. BUGA"
  }
  ,
  {
    id: 25,
    name: "Aeropuerto CALI"
  }
  ,
  {
    id: 26,
    name: "MONDOMO"
  },
  {
    id: 27,
    name: "PESCADOR"
  },
  {
    id: 29,
    name: "Ter. CARTAGENA"
  },
  {
    id: 31,
    name: "Var. GUACARI"
  },
  {
    id: 32,
    name: "Var. ZARZAL"
  },
  {
    id: 33,
    name: "LOBO GUERRERO"
  },
  {
    id: 34,
    name: "Var. SANTA ROSA DE CABAL"
  },
  {
    id: 35,
    name: "Var. CHINCHINA"
  },
  {
    id: 36,
    name: "DESCANSO INICIO"
  },
  {
    id: 37,
    name: "DESCANSO FIN"
  },
  {
    id: 38,
    name: "MANTENIMIENTO INICIO"
  },
  {
    id: 39,
    name: "MANTENIMIENTO FIN"
  },
  {
    id: 40,
    name: "PALMIRA Versalles (Cx)"
  },
  {
    id: 41,
    name: "Var. SOACHA"
  },
  {
    id: 42,
    name: "Venta Movil"
  },
  {
    id: 43,
    name: "VILLAVICENCIO"
  },
  {
    id: 44,
    name: "Ter. CUCUTA"
  },
  {
    id: 45,
    name: "Ter. CIRCASIA"
  },
  {
    id: 46,
    name: "Ter. Buga (Cx)"
  },
  {
    id: 47,
    name: "Ter. TULUA (Cx)"
  },
  {
    id: 48,
    name: "Var. LA PAILA"
  },
  {
    id: 49,
    name: "Var. CERRITO"
  },
  {
    id: 50,
    name: "Var. AMAIME"
  },
  {
    id: 51,
    name: "CARTAGO (Cx)"
  },
  {
    id: 52,
    name: "Var. SONSO"
  },
  {
    id: 53,
    name: "Var. SAN PEDRO"
  },
  {
    id: 54,
    name: "Var. BUGA PB"
  },
  {
    id: 55,
    name: "Ter. ROZO"
  },
  {
    id: 56,
    name: "ZONA FRANCA"
  },
  {
    id: 57,
    name: "Ter. CALI Cx"
  },
  {
    id: 58,
    name: "ACOPI"
  },
  {
    id: 59,
    name: "OCACIONAL1"
  },
  {
    id: 60,
    name: "OCACIONAL2"
  },
  {
    id: 61,
    name: "OCACIONAL3"
  },
  {
    id: 62,
    name: "OCACIONAL4"
  },
  {
    id: 63,
    name: "OCACIONAL5"
  },
  {
    id: 64,
    name: "OCACIONAL6"
  },
  {
    id: 65,
    name: "SAN LUIS"
  } */

]