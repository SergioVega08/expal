import xpath from 'xpath'
import { DOMParser as dom } from 'xmldom'


export function getFormatedCurrency(value: string | number): string {
    return `$${parseInt(value as string).toLocaleString('COP')}`
}

function twelveHour(hours: number): number {
    if(hours % 12 == 0){
        return hours = 12
    }
    else{
        return hours % 12
    }
}

export function getFormattedTime(value: string | number, ampm: boolean = true): string {
    const n = parseInt(value as string)
    console.log(n)
    const hours = Math.floor(n / 3600)
    const minutes = Math.floor(n % 3600 / 60)
    const ampmstr = ampm ? (hours >= 12 ? 'pm' : 'am') : ''
    return `${ampm ? twelveHour(hours) : hours}:${minutes < 10 ? '0' + minutes : minutes}${ampmstr ? ' ' + ampmstr : ''}`
}

  
export function normString(str: string) {
    return str.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
}

export function getStructHTML(html: string) {
    const doc = new dom().parseFromString(html);
    const labels = xpath.select("//h3", doc)
    const values = html.split(/<h3\b[^>]*>.*?<\/h3>/)
    // const values = xpath.select("//h3/following-sibling::*[1]", doc)

    // console.log(html)
    const listDropdown = labels.map((label: any, index) => {
        const value = values[index + 1]
        return label ? { label: label.firstChild.data, value: [value ? value.toString() : ""] } : null
    }).filter(v => !!v)

    return {
        labels,
        values,
        listDropdown,
    }
}